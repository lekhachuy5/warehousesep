﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WareHouse.Migrations;

namespace WareHouse.Models
{
    public class CategoryGroups
    {
        
        public int Id { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [StringLength(2, MinimumLength = 2, ErrorMessage = "Mã phải là 2 ký tự")]
        public string CateGroupCode { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string CateGroupName { get; set; }

        public virtual List<CategoryTypes> CategoryTypes { get; set; }
    }
}
