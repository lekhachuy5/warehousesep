﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WareHouse.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Không được để trống")]
        [RegularExpression(@"^[0-9a-zA-Z''-'\s]{1,40}$",
        ErrorMessage = "Không được để ký tự đặc biệt")]
        public string Usersname { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [RegularExpression(@"^[0-9a-zA-Z''-'\s]{1,40}$",
        ErrorMessage = "Không được để ký tự đặc biệt.")]
        public string Password { get; set; }

        [Display(Name = "Lưu thông tin tài khoản?")]
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}
