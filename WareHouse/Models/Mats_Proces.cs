﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WareHouse.Models
{
    public class Mats_Proces
    {
        public int Id { get; set; }
        [ForeignKey("Materials")]
        public int MatId { get; set; }
        [ForeignKey("Procedures")]
        public int ProceId { get; set; }
        public int LevelProce { get; set; }

        public virtual Materials Materials { get; set; }
        public virtual Procedures Procedures { get; set; }
    }
}
