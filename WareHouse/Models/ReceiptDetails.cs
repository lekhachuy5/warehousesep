﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WareHouse.Models
{
    public class ReceiptDetails
    {
        public long Id { get; set; }
        [ForeignKey("Receipt")]
        public long Receipt_Id { get; set; }
        [ForeignKey("Materials")]
        public int Materials_Id { get; set; }
        public int Duration { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        [ForeignKey("Mats_Proces")]
        public int Procedure_Materials_Id { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double TotalPrice { get; set; }
        public int Amount { get; set; }
        public virtual Receipts Receipt { get; set; }
        public virtual Materials Materials { get; set; }

        public virtual Mats_Proces Mats_Proces { get; set; }
    }
}
