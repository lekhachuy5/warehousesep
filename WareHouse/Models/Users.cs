﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WareHouse.Models
{
    public class Users
    {
       
        public string Id { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [RegularExpression(@"^[0-9a-zA-Z''-'\s]{1,40}$",
        ErrorMessage = "Không được dùng ký tự đặc biệt")]
        public string Usersname { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [RegularExpression(@"^[0-9a-zA-Z''-'\s]{1,40}$",
         ErrorMessage = "Không được dùng ký tự đặc biệt")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [EmailAddress(
         ErrorMessage = "Vui lòng nhập đúng định dạng Email")]  
       
        public string Email { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [StringLength(11, MinimumLength = 9, ErrorMessage = "Vui lòng nhập đúng định dạng số điện thoại, số điện thoại có it nhất 9 số tối đa 11 số")]
        [RegularExpression(@"^[0-9]*$",
         ErrorMessage = "Vui lòng nhập đúng định dạng số điện thoại, số điện thoại có it nhất 9 số tối đa 11 số")]  
        public string Phone { get; set; }
        public string Image { get; set; }
        public string Role { get; set; }
        public Nullable<bool> isDeactive { get; set; }
    }
}
