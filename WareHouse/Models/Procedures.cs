﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WareHouse.Models
{
    public class Procedures
    {
       
        public int Id { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string ProcedureCode { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string ProcedureName { get; set; }
        public int Quantity { get; set; }
        public string ParentCode { get; set; }

    }
}
