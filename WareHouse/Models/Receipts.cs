﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WareHouse.Models
{
    public class Receipts
    {
        public long id { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string CustomerCode { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string CustomerName { get; set; }
        public string ReceiptCode { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string LicensesIn { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string LicensesCheck { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string Receiver { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [EmailAddress(
         ErrorMessage = "Vui lòng nhập đúng định dạng Email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [RegularExpression(@"^[0-9]*$",
         ErrorMessage = "Vui lòng nhập đúng định dạng số điện thoại, số điện thoại có it nhất 9 số tối đa 11 số")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string Adress { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public DateTime ReceiveDay { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }

        public Nullable<DateTime> UpdateAt { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public double TotalPrice { get; set; }

        public string Description { get; set; }
        public string Note { get; set; }
        public int Type { get; set; }
        
        public Nullable<bool> IsEdit { get; set; }

        [JsonIgnore]
        public virtual List<ReceiptDetails> ReceiptDetails { get; set; }
    }
}
