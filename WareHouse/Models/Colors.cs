﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WareHouse.Models
{
    public class Colors
    {
      
        public int Id { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [StringLength(2, MinimumLength = 2, ErrorMessage = "Mã phải là 2 ký tự")]
        public string ColorCode { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string ColorName { get; set; }
    }
}
