﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using WareHouse.Migrations;

namespace WareHouse.Models
{
    public class Materials
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [StringLength(12, MinimumLength = 2, ErrorMessage = "Mã tối đa 12 ký tự")]
        public string MatCode { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string MatName { get; set; }
        public string Note { get; set; }
        public string Images { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string Descriptions { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string Locations { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [ForeignKey("UnitsMetrics")]
        public int Units { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [ForeignKey("Sizes")]
        public int SizesMat { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [ForeignKey("Brands")]
        public int BrandsMat { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [ForeignKey("Colors")]
        public int ColorsMat { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [ForeignKey("CategoryGroups")]
        public int GruopMat { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [ForeignKey("CategoryType")]
        public int TypeMat { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public double PrimePrice { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public double SellPrice { get; set; }

        [Required(ErrorMessage = "Không được để trống")]
        public long Quantity { get; set; }
        [DisplayName("Bảo Hành")]
        [Required(ErrorMessage = "Không được để trống")]
        public int Warranty { get; set; }
        [DisplayName("Hạn sử dụng")]
        [Required(ErrorMessage = "Không được để trống")]
        public int Duration { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdateAt { get; set; }

        public virtual Colors Colors { get; set; }
        public virtual Brands Brands { get; set; }
        public virtual Sizes Sizes { get; set; }
        public virtual UnitsMetrics UnitsMetrics { get; set; }
        public virtual CategoryGroups CategoryGroups { get; set; }
        public virtual CategoryTypes CategoryType { get; set; }

        [JsonIgnore]
        public virtual List<Mats_Proces> Mats_Process { get; set; }
    }
}
