﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WareHouse.Migrations;

namespace WareHouse.Models
{
    public class WareHous : DbContext
    {
        public WareHous(DbContextOptions<WareHous> options) : base(options)
        {

        }

        DbSet<Users> users { get; set; }
        DbSet<Colors> Colors { get; set; }
        DbSet<UnitsMetrics> UnitsMetrics { get; set; }
        DbSet<Sizes> Sizes { get; set; }
        DbSet<Brands> Brands { get; set; }

        DbSet<Procedures> Procedures { get; set; }
        DbSet<CategoryGroups> CategoryGroups { get; set; }
        DbSet<CategoryTypes> CategoryTypes { get; set; }
        DbSet<Materials> Materials { get; set; }
        DbSet<Mats_Proces> Mats_Proces { get; set; }
        DbSet<Receipts> Receipts { get; set; }
        DbSet<ReceiptDetails> ReceiptDetails { get; set; }
    }
}
