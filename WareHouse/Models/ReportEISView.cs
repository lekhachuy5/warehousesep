﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WareHouse.Models
{
    public class ReportEISView
    {
        public string code { get; set; }
        public string name { get; set; }
        public string units { get; set; }
        public int fiquantity { get; set; }
        public double fistorage { get; set; }
        public int import { get; set; }
        public double impPrice { get; set; }
        public int export { get; set; }
        public double expPrice { get; set; }
        public int laquantity { get; set; }
        public double lastorage { get; set; }

    }
}
