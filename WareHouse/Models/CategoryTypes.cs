﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WareHouse.Models
{
    public class CategoryTypes
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        [StringLength(2, MinimumLength = 2, ErrorMessage = "Mã phải là 2 ký tự")]
        public string CateTypeCode { get; set; }
        [Required(ErrorMessage = "Không được để trống")]
        public string CateTypeName { get; set; }
        public int CateLocation { get; set; }
        [Required(ErrorMessage = "Không được để trống nếu không có nhóm vật phẩm vui lòng tạo ở mục nhóm vật phẩm")]
        [ForeignKey("CategoryGroups")]
        public int Group_Id { get; set; }
        [JsonIgnore]
        public virtual CategoryGroups CategoryGroups { get; set; }
    }
}
