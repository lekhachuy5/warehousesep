﻿using System;
using System.Threading.Tasks;
using WareHouse.Models;

namespace netCoreTutorial.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private WareHous _context;

        public UnitOfWork(WareHous context)
        {
            _context = context;
            InitRepositories();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        private bool _disposed = false;

        public IGenericRepository<Users> UsersRepository { get; private set; }
        public IGenericRepository<Colors> ColorsRepository { get; private set; }
        public IGenericRepository<UnitsMetrics> UnitMetricsRepository { get; private set; }
        public IGenericRepository<Sizes> SizesRepository { get; private set; }
        public IGenericRepository<Brands> BrandsRepository { get; private set; }

        public IGenericRepository<Procedures> ProceduresRepository { get; private set; }
        public IGenericRepository<CategoryGroups> CategoryGroupsRepository { get; private set; }
        public IGenericRepository<CategoryTypes> CategoryTypesRepository { get; private set; }
        public IGenericRepository<Materials> MaterialsRepository { get; private set; }
        public IGenericRepository<Mats_Proces> Mats_ProcesRepository { get; private set; }
        public IGenericRepository<Receipts> ReceiptRepository { get; private set; }
        public IGenericRepository<ReceiptDetails> ReceiptDetailsRepository { get; private set; }
        private void InitRepositories()
        {
            UsersRepository = new GenericRepository<Users>(_context);
            ColorsRepository = new GenericRepository<Colors>(_context);
            UnitMetricsRepository = new GenericRepository<UnitsMetrics>(_context);
            SizesRepository = new GenericRepository<Sizes>(_context);
            BrandsRepository = new GenericRepository<Brands>(_context);
            ProceduresRepository = new GenericRepository<Procedures>(_context);
            CategoryGroupsRepository = new GenericRepository<CategoryGroups>(_context);
            CategoryTypesRepository = new GenericRepository<CategoryTypes>(_context);
            MaterialsRepository = new GenericRepository<Materials>(_context);
            Mats_ProcesRepository = new GenericRepository<Mats_Proces>(_context);
            ReceiptRepository = new GenericRepository<Receipts>(_context);
            ReceiptDetailsRepository = new GenericRepository<ReceiptDetails>(_context);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
