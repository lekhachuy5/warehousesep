﻿using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WareHouse.Models;

namespace netCoreTutorial.DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly WareHous _context;
        private readonly DbSet<T> _DbSet;

        public GenericRepository(WareHous context)
        {
            this._context = context;
            this._DbSet = this._context.Set<T>();
        }
        public void Add(T entity)
        {
            _DbSet.Add(entity);
        }

        public void Delete(int id)
        {
            T entityToDelete = _DbSet.Find(id);
            Delete(entityToDelete);
        }

        public bool Delete(T entityToDelete)
        {
            try
            {
                if (_context.Entry(entityToDelete).State == EntityState.Detached)
                {
                    _DbSet.Attach(entityToDelete);
                }
                _DbSet.Remove(entityToDelete);
                return true;
            }
            catch (SqlException ex) when (ex.Number == 547)
            {
             
                return false;
            }
        }
       
        public virtual IEnumerable<T> Get(
          Expression<Func<T, bool>> filter = null,
          Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
          string includeProperties = "")
        {
            IQueryable<T> query = _DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }


        public T GetByIdAsync(int id)
        {
            return _DbSet.Find(id);
        }

        public T GetOne(Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> query = _DbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query.FirstOrDefault();
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            _DbSet.RemoveRange(entities);
        }

        public void AddRange(IEnumerable<T> entities)
        {
            _DbSet.AddRange(entities);
        }
        public void Update(T entityToUpdate)
        {
            _DbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
        }
    }
}

