﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace netCoreTutorial.DAL
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");
        TEntity GetByIdAsync(int id);
        void Add(TEntity entity);
        void Delete(int id);
        bool Delete(TEntity entityToDelete);
        void Update(TEntity entityToUpdate);
        void RemoveRange(IEnumerable<TEntity> entities);
        void AddRange(IEnumerable<TEntity> entities);
        TEntity GetOne(Expression<Func<TEntity, bool>> filter = null);
    }
}
