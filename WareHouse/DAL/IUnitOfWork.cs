﻿using System;
using System.Threading.Tasks;
using WareHouse.Migrations;
using WareHouse.Models;

namespace netCoreTutorial.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<Users> UsersRepository { get; }
        IGenericRepository<Colors> ColorsRepository { get; }
        IGenericRepository<UnitsMetrics> UnitMetricsRepository { get; }
        IGenericRepository<Sizes> SizesRepository { get; }
        IGenericRepository<Brands> BrandsRepository { get; }
        IGenericRepository<Procedures> ProceduresRepository { get; }

        IGenericRepository<CategoryGroups> CategoryGroupsRepository { get; }
        IGenericRepository<CategoryTypes> CategoryTypesRepository { get; }
        IGenericRepository<Materials> MaterialsRepository { get; }
        IGenericRepository<Mats_Proces> Mats_ProcesRepository { get; }
        IGenericRepository<Receipts> ReceiptRepository { get; }
        IGenericRepository<ReceiptDetails> ReceiptDetailsRepository { get; }
        Task SaveAsync();
    }
}
