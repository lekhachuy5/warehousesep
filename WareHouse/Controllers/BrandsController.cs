﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using netCoreTutorial.DAL;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    [Authorize]
    public class BrandsController : Controller
    {
        // GET: BrandsController
        private readonly IUnitOfWork unitOfWork;

        public BrandsController(IUnitOfWork uow)
        {
            unitOfWork = uow;
        }

        public ActionResult Index(string searchKey ,int pageNumber = 1, int pageSize = 20)
        {
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
           
            var query = unitOfWork.BrandsRepository.Get(x=> !String.IsNullOrEmpty(searchKey) ? (x.BrandCode.ToLower().Contains(searchKey.ToLower()) 
            || x.BrandName.ToLower().Contains(searchKey.ToLower())
            ): true);
            var result = new PagedResult<Brands>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }

        public ActionResult Create()
        {
            ViewBag.IsCreate = true;
            return View("Form");
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(Brands Brands)
        {
            ViewBag.IsCreate = true;
            CheckAvailableCode(Brands);
            if (ModelState.IsValid)
            {
                unitOfWork.BrandsRepository.Add(Brands);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("Form", Brands);
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Brands Brands = unitOfWork.BrandsRepository.GetByIdAsync(id);
            if (Brands == null)
            {
                return NotFound();
            }
            ViewBag.Code = Brands.BrandCode;
            ViewBag.IsCreate = false;
            return View("Form", Brands);
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(Brands Brands, string code)
        {

            if (!code.Equals(Brands.BrandCode))
            {
                CheckAvailableCode(Brands);
            }

            if (ModelState.IsValid)
            {

                unitOfWork.BrandsRepository.Update(Brands);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Code = code;
            ViewBag.IsCreate = false;
            return View("Form", Brands);
        }

        private void CheckAvailableCode(Brands Brands)
        {
            Brands NameCheck = unitOfWork.BrandsRepository.GetOne(x => x.BrandCode.Equals(Brands.BrandCode));
            if (NameCheck != null)
            {
                ModelState.AddModelError("BrandCode", "Mã này đã tồn tại với tên: " + NameCheck.BrandName);
            }
        }
    }
}
