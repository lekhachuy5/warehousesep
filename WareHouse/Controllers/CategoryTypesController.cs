﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using netCoreTutorial.DAL;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    [Authorize]
    public class CategoryTypesController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public CategoryTypesController(IUnitOfWork uow)
        {

            unitOfWork = uow;
        }
        public ActionResult Index(string searchKey, int pageNumber = 1, int pageSize = 20)
        {
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.CategoryTypesRepository.Get(x => !String.IsNullOrEmpty(searchKey) ? (x.CateTypeCode.ToLower().Contains(searchKey.ToLower())
            || x.CateTypeName.ToLower().Contains(searchKey.ToLower())
            ) : true);
            var result = new PagedResult<CategoryTypes>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }

        public ActionResult Create()
        {
            ViewBag.IsCreate = true;
            PopularData();
            return View("Form");
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(CategoryTypes procedures)
        {
            ViewBag.IsCreate = true;
            CheckAvailableCode(procedures);
            if (ModelState.IsValid)
            {
                procedures.CateLocation = unitOfWork.CategoryTypesRepository.Get(x => x.Group_Id == procedures.Group_Id).Count()+1;
                unitOfWork.CategoryTypesRepository.Add(procedures);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            PopularData(procedures.Group_Id);
            return View("Form", procedures);
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            CategoryTypes procedures = unitOfWork.CategoryTypesRepository.GetByIdAsync(id);
            if (procedures == null)
            {
                return NotFound();
            }
            ViewBag.Code = procedures.CateTypeCode;
            ViewBag.IsCreate = false;
            PopularData(procedures.Group_Id);
            return View("Form", procedures);
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(CategoryTypes procedures, string code)
        {

            if (!code.Equals(procedures.CateTypeCode))
            {
                CheckAvailableCode(procedures);
            }

            if (ModelState.IsValid)
            {

                unitOfWork.CategoryTypesRepository.Update(procedures);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Code = code;
            PopularData(procedures.Group_Id);
            ViewBag.IsCreate = false;
            return View("Form", procedures);
        }

        private void CheckAvailableCode(CategoryTypes procedures)
        {
            CategoryTypes NameCheck = unitOfWork.CategoryTypesRepository.GetOne(x => x.CateTypeCode.Equals(procedures.CateTypeCode));
            if (NameCheck != null)
            {
                ModelState.AddModelError("CateTypeCode", "Mã này đã tồn tại với tên: " + NameCheck.CateTypeName);
            }
        }

        private void PopularData(object group_id = null)
        {
            ViewBag.Group_Id = new SelectList(unitOfWork.CategoryGroupsRepository.Get(), "Id", "CateGroupName", group_id);
        }
    }
}
