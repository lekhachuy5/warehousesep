﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using netCoreTutorial.DAL;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    [Authorize]
    public class CategoryGroupsController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public CategoryGroupsController(IUnitOfWork uow)
        {

            unitOfWork = uow;
        }
        public ActionResult Index(string searchKey, int pageNumber = 1, int pageSize = 20)
        {
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.CategoryGroupsRepository.Get(x => !String.IsNullOrEmpty(searchKey) ? (x.CateGroupCode.ToLower().Contains(searchKey.ToLower())
            || x.CateGroupName.ToLower().Contains(searchKey.ToLower())
            ) : true);
            var result = new PagedResult<CategoryGroups>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }

        public ActionResult Create()
        {
            ViewBag.IsCreate = true;
            return View("Form");
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(CategoryGroups procedures)
        {
            ViewBag.IsCreate = true;
            CheckAvailableCode(procedures);
            if (ModelState.IsValid)
            {
                unitOfWork.CategoryGroupsRepository.Add(procedures);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("Form", procedures);
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            CategoryGroups procedures = unitOfWork.CategoryGroupsRepository.GetByIdAsync(id);
            if (procedures == null)
            {
                return NotFound();
            }
            ViewBag.Code = procedures.CateGroupCode;
            ViewBag.IsCreate = false;
            return View("Form", procedures);
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(CategoryGroups procedures, string code)
        {

            if (!code.Equals(procedures.CateGroupCode))
            {
                CheckAvailableCode(procedures);
            }

            if (ModelState.IsValid)
            {

                unitOfWork.CategoryGroupsRepository.Update(procedures);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Code = code;
            ViewBag.IsCreate = false;
            return View("Form", procedures);
        }

        private void CheckAvailableCode(CategoryGroups procedures)
        {
            CategoryGroups NameCheck = unitOfWork.CategoryGroupsRepository.GetOne(x => x.CateGroupCode.Equals(procedures.CateGroupCode));
            if (NameCheck != null)
            {
                ModelState.AddModelError("CateGroupCode", "Mã này đã tồn tại với tên: " + NameCheck.CateGroupName);
            }
        }
    }
}
