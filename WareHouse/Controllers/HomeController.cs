﻿using System.Composition;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using netCoreTutorial.DAL;
using WareHouse.Helpers;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly int[] impo = { Constants.IMPORT, Constants.REIMPORT };
        public HomeController(IUnitOfWork uow)
        {
            unitOfWork = uow;
        }

        public async Task<ActionResult> Index()
        {
            var Receipt = unitOfWork.ReceiptRepository.Get();
            var rede = unitOfWork.ReceiptDetailsRepository.Get();

            var statis = new StatisticViewModel
            {
                countExportReceipt = rede.Where(x => x.Receipt.Type == Constants.EXPORT).Select(x => x.Amount).Sum(),
                countImportReceipt = rede.Where(x => impo.Contains(x.Receipt.Type)).Select(x => x.Amount).Sum(),
                countAll = rede.Select(x => x.Amount).Sum()

            };
            return View(statis);
        }

        public IActionResult Privacy()
        {
            return View();
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
