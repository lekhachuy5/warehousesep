﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Castle.Core.Internal;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using netCoreTutorial.DAL;
using WareHouse.Helpers;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    [Authorize]
    public class ReceiptController : Controller
    {
        // GET: ReceiptController

        private readonly IUnitOfWork unitOfWork;
        public ReceiptController(IUnitOfWork uow)
        {

            unitOfWork = uow;
        }
        public ActionResult Exports(string searchKey, DateTime? dateSearch,int pageNumber = 1, int pageSize = 20)
        {
            ViewBag.Actions = "Exports";
            ViewBag.Title = Constants.TITLESWITCH[Constants.EXPORT];
            ViewBag.Type = Constants.EXPORT;
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.ReceiptRepository.Get(x => x.Type == Constants.EXPORT && (!String.IsNullOrEmpty(searchKey) ?
            x.ReceiptCode.ToLower().Contains(searchKey.ToLower()) : true) 
            && (dateSearch.HasValue ? (x.CreatedAt.Value.Date.Equals(dateSearch.Value.Date) || x.UpdateAt.Value.Date.Equals(dateSearch.Value.Date)):true)
            );
            var result = new PagedResult<Receipts>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).OrderByDescending(x => x.CreatedAt).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View("Index", result);
        }
        public ActionResult Imports(string searchKey, DateTime? dateSearch, int pageNumber = 1, int pageSize = 20)
        {
            ViewBag.Actions = "Imports";
            ViewBag.Type = Constants.IMPORT;
            ViewBag.Title = Constants.TITLESWITCH[Constants.IMPORT];
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.ReceiptRepository.Get(x => x.Type == Constants.IMPORT && (!String.IsNullOrEmpty(searchKey) ?
            x.ReceiptCode.ToLower().Contains(searchKey.ToLower()) : true)
            && (dateSearch.HasValue ? (x.CreatedAt.Value.Date.Equals(dateSearch.Value.Date) || x.UpdateAt.Value.Date.Equals(dateSearch.Value.Date)) : true));
            var result = new PagedResult<Receipts>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).OrderByDescending(x => x.CreatedAt).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View("Index", result);
        }
        public ActionResult ReImports(string searchKey, DateTime? dateSearch, int pageNumber = 1, int pageSize = 20)
        {
            ViewBag.Actions = "ReImports";
            ViewBag.Type = Constants.REIMPORT;
            ViewBag.Title = Constants.TITLESWITCH[Constants.REIMPORT];
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.ReceiptRepository.Get(x => x.Type == Constants.REIMPORT && (!String.IsNullOrEmpty(searchKey) ?
            x.ReceiptCode.ToLower().Contains(searchKey.ToLower()) : true)
            && (dateSearch.HasValue ? (x.CreatedAt.Value.Date.Equals(dateSearch.Value.Date) || x.UpdateAt.Value.Date.Equals(dateSearch.Value.Date)) : true));
            var result = new PagedResult<Receipts>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).OrderByDescending(x => x.CreatedAt).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View("Index", result);
        }

        // GET: ReceiptController/Details/5
        public ActionResult Print(long id)
        {
            PopularData();
            return View(unitOfWork.ReceiptRepository.GetOne(x => x.id == id));
        }

        // GET: ReceiptController/Create
        public ActionResult Create()
        {
            ViewBag.IsCreate = true;
            Receipts ci = new Receipts
            {
                ReceiptDetails = new List<ReceiptDetails> { new ReceiptDetails {
                Id = 0, Materials_Id = 1, Quantity = 0, Procedure_Materials_Id = 1,Price = 0, TotalPrice=0 , Amount =0} }
            };
            PopularData();
            return View("Form", ci);
        }

        // POST: ReceiptController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(Receipts receipt, string actions, int type)
        {
            ViewBag.IsCreate = true;
            ViewBag.Actions = actions;
            ViewBag.Type = receipt.Type;
            PopularData();
            receipt.ReceiptCode = Constants.CODESWITCH[type] + DateTime.Now.ToString("ddMMyyyy") + DateTime.Now.ToString("hhmmss");
            receipt.Type = type;
            receipt.IsEdit = true;
            if (receipt.Type == Constants.EXPORT)
            {
                int i = 0;
                foreach (var item in receipt.ReceiptDetails)
                {
                    var materials = unitOfWork.MaterialsRepository.GetByIdAsync(item.Materials_Id);
                    item.Receipt_Id = receipt.id;
                    item.Duration = materials.Duration;
                    item.CreatedAt = DateTime.Now;
                    if (Convert.ToInt32(materials.Quantity) >= item.Amount)
                    {
                        materials.Quantity = materials.Quantity - item.Amount;
                        unitOfWork.MaterialsRepository.Update(materials);
                        unitOfWork.ReceiptDetailsRepository.Add(item);
                    }
                    else
                    {
                        ModelState.AddModelError("ReceiptDetails[" + i + "].Amount", materials.MatName + "(" + materials.MatCode + ")" + " Không đủ số lượng để xuất (số lượng xuất: " + item.Amount + " số lượng Tồn: " + materials.Quantity);
                    }
                    i++;
                }
                if (ModelState.IsValid)
                {
                    receipt.UpdateAt = DateTime.Now;
                    receipt.CreatedAt = DateTime.Now;
                    unitOfWork.ReceiptRepository.Add(receipt);
                    await unitOfWork.SaveAsync();
                    return RedirectToAction(actions);
                }

                return View("Form", receipt);
            }
            else
            {
                foreach (var item in receipt.ReceiptDetails)
                {
                    var materials = unitOfWork.MaterialsRepository.GetByIdAsync(item.Materials_Id);
                    item.Receipt_Id = receipt.id;
                    item.Duration = materials.Duration;
                    item.CreatedAt = DateTime.Now;
                    materials.Quantity = materials.Quantity + item.Amount;
                    unitOfWork.MaterialsRepository.Update(materials);
                    unitOfWork.ReceiptDetailsRepository.Add(item);
                }
                if (ModelState.IsValid)
                {
                    receipt.UpdateAt = DateTime.Now;
                    receipt.CreatedAt = DateTime.Now;
                    unitOfWork.ReceiptRepository.Add(receipt);
                    await unitOfWork.SaveAsync();
                    return RedirectToAction(actions);
                }

                return View("Form", receipt);
            }
        }
        // GET: ReceiptController/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Receipts receipts = unitOfWork.ReceiptRepository.GetOne(x => x.id == id);
            unitOfWork.ReceiptDetailsRepository.Get();
            if (receipts == null || receipts.IsEdit != true)
            {
                return NotFound();
            }
            ViewBag.IsCreate = false;
            PopularData();
            return View("Form", receipts);

        }

        // POST: ReceiptController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(Receipts receipt, string actions, int type)
        {
            ViewBag.IsCreate = false;
            ViewBag.Actions = actions;
            PopularData();

            ViewBag.Type = type;
            receipt.Type = type;
            receipt.IsEdit = true;
            var removeMat = unitOfWork.ReceiptDetailsRepository.Get(x => x.Receipt_Id == receipt.id);
            int[] oldquantity = removeMat.Select(x => x.Amount).ToArray();
           
            if (receipt.Type == Constants.EXPORT)
            {
                int i = 0;
                foreach (var item in receipt.ReceiptDetails)
                {
                    var materials = unitOfWork.MaterialsRepository.GetByIdAsync(item.Materials_Id);
                    item.Receipt_Id = receipt.id;
                    item.Duration = materials.Duration;
                    item.CreatedAt = receipt.CreatedAt;
                    if (Convert.ToInt32(materials.Quantity + oldquantity[i]) >= item.Amount)
                    {
                        materials.Quantity = materials.Quantity + oldquantity[i] - item.Amount;
                        unitOfWork.MaterialsRepository.Update(materials);
                        unitOfWork.ReceiptDetailsRepository.Add(item);
                    }
                    else
                    {
                        ModelState.AddModelError("ReceiptDetails[" + i + "].Amount", materials.MatName + "(" + materials.MatCode + ")" + " Không đủ số lượng để xuất (số lượng xuất: " + item.Amount + " số lượng Tồn: " + materials.Quantity);
                        PopularData();
                    }
                    i++;
                }
                if (ModelState.IsValid)
                {
                    receipt.UpdateAt = DateTime.Now;
                    unitOfWork.ReceiptDetailsRepository.RemoveRange(removeMat);
                    unitOfWork.ReceiptRepository.Update(receipt);
                    await unitOfWork.SaveAsync();
                    return RedirectToAction(actions);
                }
                return View("Form", receipt);
            }
            else
            {
                int i = 0;
                foreach (var item in receipt.ReceiptDetails)
                {

                    var materials = unitOfWork.MaterialsRepository.GetByIdAsync(item.Materials_Id);
                    item.Receipt_Id = receipt.id;
                    item.Duration = materials.Duration;
                    item.CreatedAt = receipt.CreatedAt;
                    if (Convert.ToInt32(materials.Quantity + item.Amount) >= oldquantity[i])
                    {
                        materials.Quantity = materials.Quantity + item.Amount - oldquantity[i];
                    }
                    else
                    {
                        ModelState.AddModelError("ReceiptDetails[" + i + "].Amount", materials.MatName + "(" + materials.MatCode + ")" + " Không đủ số lượng để cập nhật (số lượng sau cập nhật: " + (materials.Quantity + item.Amount - oldquantity[i]));
                    }
                    unitOfWork.MaterialsRepository.Update(materials);
                    unitOfWork.ReceiptDetailsRepository.Add(item);
                    i++;
                }
                if (ModelState.IsValid)
                {
                    receipt.UpdateAt = DateTime.Now;
                    unitOfWork.ReceiptDetailsRepository.RemoveRange(removeMat);
                    unitOfWork.ReceiptRepository.Update(receipt);
                    await unitOfWork.SaveAsync();
                    return RedirectToAction(actions);
                }


                return View("Form", receipt);
            }
        }

        // GET: ReceiptController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ReceiptController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        private void PopularData()
        {
            ViewBag.Materials = unitOfWork.MaterialsRepository.Get().Select(x => new Materials
            {
                Id = x.Id,
                MatName = x.MatName + " (" + x.MatCode + ")"
            });
            ViewBag.Procedure_Materials_Id = unitOfWork.Mats_ProcesRepository.Get().OrderBy(x => x.LevelProce).Select(x => new Procedures
            {
                Id = x.Id,
                ProcedureName = x.Procedures.ProcedureName + " (" + x.Procedures.Quantity + " Đơn vị)"
            });
        }

        public JsonResult GetDistrictByCityId(int id)
        {
            // Disable proxy creation

            var listDistrict = unitOfWork.Mats_ProcesRepository.Get(x => x.MatId == id).Select(x => new Procedures
            {
                Id = x.Id,
                ProcedureName = x.Procedures.ProcedureName + " (" + x.Procedures.Quantity + " Đơn vị)"
            });
            return Json(listDistrict.ToList());
        }

        public JsonResult GetPrice(int id)
        {
            var listDistrict = unitOfWork.MaterialsRepository.GetByIdAsync(id);
            return Json(listDistrict.SellPrice);
        }

        public JsonResult GetProcedureAmount(int id)
        {
            int count = 1;
            var listDistrict = unitOfWork.Mats_ProcesRepository.Get();
            var matid = listDistrict.FirstOrDefault(x => x.Id == id);
            var newlist = listDistrict.Where(x => x.MatId == matid.MatId && x.LevelProce >= matid.LevelProce).Select(x => new Mats_Proces
            {
                Id = x.Id,
                LevelProce = x.LevelProce,
                Procedures = x.Procedures
            }).OrderBy(x => x.LevelProce);
            foreach (var item in newlist)
            {
                count = count * item.Procedures.Quantity;
            }
            return Json(count);
        }

        private void CheckAmount(Materials materials, int quantity)
        {

            if (materials.Quantity < quantity)
            {
                ModelState.AddModelError("ReceiptCode", materials.MatName + "(" + materials.MatCode + ")" + " Không đủ số lượng để xuất (số lượng xuất: " + quantity + " số lượng nhập: " + materials.Quantity);
            }
        }

        [HttpPost]
        public async Task<JsonResult> AdjustAmg(DateTime mydate)
        {
            //if (!String.IsNullOrEmpty(strDate))
            //{
            //    DateTime date = Convert.ToDateTime(strDate);

            var receitp = unitOfWork.ReceiptRepository.Get();
            foreach (var i in receitp)
            {
             
                if (i.CreatedAt.Value > mydate)
                {
                    i.IsEdit = true;
                }
                else
                {
                    i.IsEdit = false;
                }
                unitOfWork.ReceiptRepository.Update(i);
            }



            await unitOfWork.SaveAsync();
            return this.Json(true);
        }
        //    else
        //    {
        //        return this.Json(false);
        //    }
        //}
    }
}
