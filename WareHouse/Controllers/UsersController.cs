﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using netCoreTutorial.DAL;
using WareHouse.Models;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using WareHouse.Helpers;

namespace WareHouse.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IWebHostEnvironment hostEnvironment;
        public UsersController(IUnitOfWork uow, IWebHostEnvironment hostEnvironment)
        {
            this.hostEnvironment = hostEnvironment;
            unitOfWork = uow;
        }
        [Authorize(Roles = Constants.ADMINS)]
        public ActionResult Index(int pageNumber = 1, int pageSize = 20)
        {
            if (User.IsInRole(Constants.ADMIN))
            {
                int ExcludeRecords = (pageSize * pageNumber) - pageSize;
                var query = unitOfWork.UsersRepository.Get(x => x.isDeactive == false).Skip(ExcludeRecords).Take(pageSize);
                var result = new PagedResult<Users>
                {
                    Data = query.ToList(),
                    TotalItems = unitOfWork.UsersRepository.Get().Count(),
                    PageNumber = pageNumber,
                    PageSize = pageSize
                };
                return View(result);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [Authorize(Roles = Constants.ADMINS)]
        public ActionResult Create()
        {
            if (User.IsInRole(Constants.ADMIN))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // POST: EmployeeController/Create
        [Authorize(Roles = Constants.ADMINS)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(Users users, IFormFile imgFiles)
        {
            CheckAvailableUserName(users);
            if (ModelState.IsValid)
            {
                string wwwRootPath = hostEnvironment.WebRootPath;
                string filename = Path.GetFileNameWithoutExtension(imgFiles.FileName);
                string extension = Path.GetExtension(imgFiles.FileName);
                filename = filename + DateTime.Now.ToString("yymmssfff") + extension;

                users.Image = filename;
                string path = Path.Combine(wwwRootPath + "/Image", filename);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await imgFiles.CopyToAsync(stream);
                }

                users.Id = Guid.NewGuid().ToString();
                users.isDeactive = false;
                unitOfWork.UsersRepository.Add(users);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(users);
        }
        [Authorize(Roles = Constants.ADMINS)]
        public ActionResult Edit(string id)
        {
            if (User.IsInRole(Constants.ADMIN))
            {
                if (string.IsNullOrEmpty(id))
                {
                    return BadRequest();
                }
                Users users = unitOfWork.UsersRepository.GetOne(x => x.Id.Equals(id));
                if (users == null)
                {
                    return NotFound();
                }
                ViewBag.Code = users.Usersname;
                return View(users);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest();
            }
            Users users = unitOfWork.UsersRepository.GetOne(x => x.Id.Equals(id));
            if (users == null)
            {
                return NotFound();
            }
            ViewBag.Code = users.Usersname;
            return View(users);
        }
        // POST: EmployeeController/Create
        [Authorize(Roles = Constants.ADMINS)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(Users users, IFormFile imgFiles, string code)
        {
            if (!code.Equals(users.Usersname))
            {
                CheckAvailableUserName(users);
            }

            if (ModelState.IsValid)
            {

                if (imgFiles != null)
                {
                    string oldImage = users.Image;
                    var imagePath = Path.Combine(hostEnvironment.WebRootPath, "Image", oldImage);
                    if (System.IO.File.Exists(imagePath))
                    {
                        System.IO.File.Delete(imagePath);
                    }
                    string wwwRootPath = hostEnvironment.WebRootPath;
                    string filename = Path.GetFileNameWithoutExtension(imgFiles.FileName);
                    string extension = Path.GetExtension(imgFiles.FileName);
                    filename = filename + DateTime.Now.ToString("yymmssfff") + extension;

                    users.Image = filename;
                    string path = Path.Combine(wwwRootPath + "/Image", filename);
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await imgFiles.CopyToAsync(stream);
                    }
                }
                unitOfWork.UsersRepository.Update(users);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Code = code;
            return View(users);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DetailsAsync(Users users, IFormFile imgFiles, string code)
        {
            if (!code.Equals(users.Usersname))
            {
                CheckAvailableUserName(users);
            }

            if (ModelState.IsValid)
            {

                if (imgFiles != null)
                {
                    string oldImage = users.Image;
                    var imagePath = Path.Combine(hostEnvironment.WebRootPath, "Image", oldImage);
                    if (System.IO.File.Exists(imagePath))
                    {
                        System.IO.File.Delete(imagePath);
                    }
                    string wwwRootPath = hostEnvironment.WebRootPath;
                    string filename = Path.GetFileNameWithoutExtension(imgFiles.FileName);
                    string extension = Path.GetExtension(imgFiles.FileName);
                    filename = filename + DateTime.Now.ToString("yymmssfff") + extension;

                    users.Image = filename;
                    string path = Path.Combine(wwwRootPath + "/Image", filename);
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await imgFiles.CopyToAsync(stream);
                    }
                }
                unitOfWork.UsersRepository.Update(users);
                await unitOfWork.SaveAsync();
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Code = code;
            return View(users);
        }
        [Authorize(Roles = Constants.ADMINS)]
        public ActionResult Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest();
            }
            Users users = unitOfWork.UsersRepository.GetOne(x => x.Id.Equals(id));
            if (users == null)
            {
                return NotFound();
            }
            return View(users);
        }

        // POST: ColorsController/Delete/5
        [Authorize(Roles = Constants.ADMINS)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAsync(string id)
        {
            Users users = unitOfWork.UsersRepository.GetOne(x => x.Id.Equals(id));
            users.isDeactive = true;
            unitOfWork.UsersRepository.Update(users);
            await unitOfWork.SaveAsync();
            return RedirectToAction(nameof(Index));
        }

        private void CheckAvailableUserName(Users users)
        {
            int userNameList = unitOfWork.UsersRepository.Get(x => x.Usersname.Equals(users.Usersname) && x.isDeactive == false).Count();
            if (userNameList > 0)
            {
                ModelState.AddModelError("Usersname", "Tên đăng nhập đã tồn tại trong hệ thống");
            }
        }
    }
}
