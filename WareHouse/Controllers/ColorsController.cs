﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using netCoreTutorial.DAL;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    [Authorize]
    public class ColorsController : Controller
    {
        // GET: ColorsController
        private readonly IUnitOfWork unitOfWork;

        public ColorsController(IUnitOfWork uow)
        {
            unitOfWork = uow;
        }

        public ActionResult Index(string searchKey, int pageNumber = 1, int pageSize = 20)
        {
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.ColorsRepository.Get(x => !String.IsNullOrEmpty(searchKey) ? (x.ColorCode.ToLower().Contains(searchKey.ToLower())
            || x.ColorName.ToLower().Contains(searchKey.ToLower())
            ) : true);
            var result = new PagedResult<Colors>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }

        public ActionResult Create()
        {
            ViewBag.IsCreate = true;
            return View("Form");
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(Colors colors)
        {
            ViewBag.IsCreate = true;
            CheckAvailableCode(colors);
            if (ModelState.IsValid)
            {
                unitOfWork.ColorsRepository.Add(colors);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("Form", colors);
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Colors colors = unitOfWork.ColorsRepository.GetByIdAsync(id);
            if (colors == null)
            {
                return NotFound();
            }
            ViewBag.Code = colors.ColorCode;
            ViewBag.IsCreate = false;
            return View("Form", colors);
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(Colors colors, string code)
        {

            if (!code.Equals(colors.ColorCode))
            {
                CheckAvailableCode(colors);
            }

            if (ModelState.IsValid)
            {

                unitOfWork.ColorsRepository.Update(colors);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Code = code;
            ViewBag.IsCreate = false;
            return View("Form", colors);
        }

        private void CheckAvailableCode(Colors colors)
        {
            Colors NameCheck = unitOfWork.ColorsRepository.GetOne(x => x.ColorCode.Equals(colors.ColorCode));
            if (NameCheck != null)
            {
                ModelState.AddModelError("ColorCode", "Mã này đã tồn tại với tên: " + NameCheck.ColorName);
            }
        }
    }
}
