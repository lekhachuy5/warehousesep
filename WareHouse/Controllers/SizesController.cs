﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using netCoreTutorial.DAL;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    [Authorize]
    public class SizesController : Controller
    {
        // GET: SizesController
        private readonly IUnitOfWork unitOfWork;

        public SizesController(IUnitOfWork uow)
        {
            unitOfWork = uow;
        }

        public ActionResult Index(string searchKey,int pageNumber = 1, int pageSize = 20)
        {
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.SizesRepository.Get(x => !String.IsNullOrEmpty(searchKey) ? (x.SizeName.ToLower().Contains(searchKey.ToLower())
            || x.SizeCode.ToLower().Contains(searchKey.ToLower())
            ) : true);
            var result = new PagedResult<Sizes>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }

        public ActionResult Create()
        {
            ViewBag.IsCreate = true;
            return View("Form");
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(Sizes sizes)
        {
            ViewBag.IsCreate = true;
            CheckAvailableCode(sizes);
            if (ModelState.IsValid)
            {
                unitOfWork.SizesRepository.Add(sizes);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("Form", sizes);
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Sizes Sizes = unitOfWork.SizesRepository.GetByIdAsync(id);
            if (Sizes == null)
            {
                return NotFound();
            }
            ViewBag.Code = Sizes.SizeCode;
            ViewBag.IsCreate = false;
            return View("Form", Sizes);
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(Sizes Sizes, string code)
        {

            if (!code.Equals(Sizes.SizeCode))
            {
                CheckAvailableCode(Sizes);
            }

            if (ModelState.IsValid)
            {

                unitOfWork.SizesRepository.Update(Sizes);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Code = code;
            ViewBag.IsCreate = false;
            return View("Form", Sizes);
        }

        private void CheckAvailableCode(Sizes Sizes)
        {
            Sizes NameCheck = unitOfWork.SizesRepository.GetOne(x => x.SizeCode.Equals(Sizes.SizeCode));
            if (NameCheck != null)
            {
                ModelState.AddModelError("SizeCode", "Mã này đã tồn tại với tên: " + NameCheck.SizeName);
            }
        }
    }
}
