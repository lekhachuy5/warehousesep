﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using netCoreTutorial.DAL;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    [Authorize]
    public class UnitsMetricsController : Controller
    {
        // GET: UnitsMetricsController
        private readonly IUnitOfWork unitOfWork;

        public UnitsMetricsController(IUnitOfWork uow)
        {
            unitOfWork = uow;
        }

        public ActionResult Index(string searchKey,int pageNumber = 1, int pageSize = 20)
        {
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.UnitMetricsRepository.Get(x => !String.IsNullOrEmpty(searchKey) ? (x.UnitsName.ToLower().Contains(searchKey.ToLower())
            || x.UnitsCode.ToLower().Contains(searchKey.ToLower())
            ) : true);
            var result = new PagedResult<UnitsMetrics>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }

        public ActionResult Create()
        {
            ViewBag.IsCreate = true;
            return View("Form");
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(UnitsMetrics UnitsMetrics)
        {
            ViewBag.IsCreate = true;
            CheckAvailableCode(UnitsMetrics);
            if (ModelState.IsValid)
            {
                unitOfWork.UnitMetricsRepository.Add(UnitsMetrics);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("Form", UnitsMetrics);
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            UnitsMetrics UnitsMetrics = unitOfWork.UnitMetricsRepository.GetByIdAsync(id);
            if (UnitsMetrics == null)
            {
                return NotFound();
            }
            ViewBag.Code = UnitsMetrics.UnitsCode;
            ViewBag.IsCreate = false;
            return View("Form", UnitsMetrics);
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(UnitsMetrics UnitsMetrics, string code)
        {

            if (!code.Equals(UnitsMetrics.UnitsCode))
            {
                CheckAvailableCode(UnitsMetrics);
            }

            if (ModelState.IsValid)
            {

                unitOfWork.UnitMetricsRepository.Update(UnitsMetrics);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Code = code;
            ViewBag.IsCreate = false;
            return View("Form", UnitsMetrics);
        }

        private void CheckAvailableCode(UnitsMetrics UnitsMetrics)
        {
            UnitsMetrics NameCheck = unitOfWork.UnitMetricsRepository.GetOne(x => x.UnitsCode.Equals(UnitsMetrics.UnitsCode));
            if (NameCheck != null)
            {
                ModelState.AddModelError("UnitsCode", "Mã này đã tồn tại với tên: " + NameCheck.UnitsName);
            }
        }
    }
}
