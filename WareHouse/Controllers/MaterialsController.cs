﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using netCoreTutorial.DAL;
using WareHouse.Models;
using WareHouse.Helpers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace WareHouse.Controllers
{
    [Authorize]
    public class MaterialsController : Controller
    {
        // GET: MaterialsController
        private readonly IUnitOfWork unitOfWork;
        private readonly IWebHostEnvironment hostEnvironment;
        public MaterialsController(IUnitOfWork uow, IWebHostEnvironment hostEnvironment)
        {
            this.hostEnvironment = hostEnvironment;
            unitOfWork = uow;
        }
        public ActionResult Index(string searchKey, int pageNumber = 1, int pageSize = 20)
        {
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.MaterialsRepository.Get(x => !String.IsNullOrEmpty(searchKey) ? (x.MatCode.ToLower().Contains(searchKey.ToLower())
            || x.MatName.ToLower().Contains(searchKey.ToLower())
            ) : true);
            var result = new PagedResult<Materials>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }

        // GET: MaterialsController/Create
        public ActionResult Create()
        {
            ViewBag.IsCreate = true;
            Materials ci = new Materials { Mats_Process = new List<Mats_Proces> { new Mats_Proces { Id = 0, ProceId = 1, MatId = 1, LevelProce = 0 } } };
            PopularData();
            return View("Form", ci);
        }

        // POST: MaterialsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(Materials materials, IFormFile imgFiles)
        {
            ViewBag.IsCreate = true;
            //CheckAvailableCode(procedures);
            CheckAvailableCode(materials);
            if (ModelState.IsValid)
            {
                materials.UpdateAt = DateTime.Now;
                materials.CreatedAt = DateTime.Now;
                string wwwRootPath = hostEnvironment.WebRootPath;
                string filename = Path.GetFileNameWithoutExtension(imgFiles.FileName);
                string extension = Path.GetExtension(imgFiles.FileName);
                filename = filename + DateTime.Now.ToString("yymmssfff") + extension;

                materials.Images = filename;
                string path = Path.Combine(wwwRootPath + "/Image", filename);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await imgFiles.CopyToAsync(stream);
                }
                foreach (var item in materials.Mats_Process)
                {
                    item.MatId = materials.Id;
                    unitOfWork.Mats_ProcesRepository.Add(item);
                }
                unitOfWork.MaterialsRepository.Add(materials);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            PopularData();
            return View("Form", materials);
        }

        // GET: MaterialsController/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Materials materials = unitOfWork.MaterialsRepository.GetByIdAsync(id);
            if (materials == null)
            {
                return NotFound();
            }
            PopularData(materials.Units, materials.ColorsMat, materials.SizesMat, materials.BrandsMat, materials.GruopMat, materials.TypeMat);
            ViewBag.Code = materials.MatCode;
            ViewBag.IsCreate = false;
            ViewBag.OldPrice = materials.PrimePrice;
            return View("Form", materials);
        }

        // POST: MaterialsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(Materials materials, IFormFile imgFiles, string code, double oldPrice)
        {
            ViewBag.IsCreate = false;
            if (!code.Equals(materials.MatCode))
            {
                CheckAvailableCode(materials);
            }
           
            if (ModelState.IsValid)
            {
                materials.UpdateAt = DateTime.Now;
                if(oldPrice != materials.PrimePrice) {
                    materials.SellPrice = (materials.SellPrice + materials.PrimePrice) / 2;
                
                }
                if (imgFiles != null)
                {
                    string oldImage = materials.Images;
                    var imagePath = Path.Combine(hostEnvironment.WebRootPath, "Image", oldImage);
                    if (System.IO.File.Exists(imagePath))
                    {
                        System.IO.File.Delete(imagePath);
                    }
                    string wwwRootPath = hostEnvironment.WebRootPath;
                    string filename = Path.GetFileNameWithoutExtension(imgFiles.FileName);
                    string extension = Path.GetExtension(imgFiles.FileName);
                    filename = filename + DateTime.Now.ToString("yymmssfff") + extension;

                    materials.Images = filename;
                    string path = Path.Combine(wwwRootPath + "/Image", filename);
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await imgFiles.CopyToAsync(stream);
                    }
                }
                unitOfWork.MaterialsRepository.Update(materials);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Code = code;
            ViewBag.OldPrice = oldPrice;
            PopularData(materials.Units, materials.ColorsMat, materials.SizesMat, materials.BrandsMat, materials.GruopMat, materials.TypeMat);
            return View("Form", materials);
        }

        private void PopularData(object units = null, object colors = null, object sizes = null, object brands = null, object groups = null, object types = null)
        {
            ViewBag.Units = new SelectList(unitOfWork.UnitMetricsRepository.Get(), "Id", "UnitsName", units);
            ViewBag.ColorsMat = new SelectList(unitOfWork.ColorsRepository.Get(), "Id", "ColorName", colors);
            ViewBag.SizesMat = new SelectList(unitOfWork.SizesRepository.Get(), "Id", "SizeName", sizes);
            ViewBag.BrandsMat = new SelectList(unitOfWork.BrandsRepository.Get(), "Id", "BrandName", brands);
            ViewBag.TypeMat = new SelectList(unitOfWork.CategoryTypesRepository.Get(), "Id", "CateTypeName", types);
            ViewBag.GruopMat = new SelectList(unitOfWork.CategoryGroupsRepository.Get(), "Id", "CateGroupName", groups);
            ViewBag.Procedures = unitOfWork.ProceduresRepository.Get().Select(x=> new Procedures {
                Id =x.Id,
                ProcedureCode = x.ProcedureCode,
                ProcedureName = x.ProcedureName + " ("+x.Quantity+" Đơn vị)"
            });
        }

        public JsonResult GetColorsCode(int id)
        {
            var listDistrict = unitOfWork.ColorsRepository.GetByIdAsync(id);
            return Json(listDistrict.ColorCode);
        }
        public JsonResult GetSizesCode(int id)
        {
            var listDistrict = unitOfWork.SizesRepository.GetByIdAsync(id);
            return Json(listDistrict.SizeCode);
        }
        public JsonResult GetBrandsCode(int id)
        {
            var listDistrict = unitOfWork.BrandsRepository.GetByIdAsync(id);
            return Json(listDistrict.BrandCode);
        }
        public JsonResult GetTypesCode(int id)
        {
            var listDistrict = unitOfWork.CategoryTypesRepository.GetByIdAsync(id);
            return Json(listDistrict.CateTypeCode);
        }
        public JsonResult GetGroupsCode(int id)
        {
            var listDistrict = unitOfWork.CategoryTypesRepository.GetByIdAsync(id);
            return Json(listDistrict.CateLocation.ToString("00"));
        }

        public JsonResult GetCodeWhenCreate()
        {
            string code = unitOfWork.CategoryTypesRepository.GetOne().CateTypeCode +
                unitOfWork.SizesRepository.GetOne().SizeCode +
                unitOfWork.BrandsRepository.GetOne().BrandCode +
                unitOfWork.ColorsRepository.GetOne().ColorCode +
                unitOfWork.CategoryTypesRepository.GetOne().CateLocation
                ;
            return Json(code);
        }
        public JsonResult GetDistrictByCityId(int id)
        {
            // Disable proxy creation

            var listDistrict = unitOfWork.CategoryTypesRepository.Get(x => x.Group_Id == id).Select(x => new CategoryTypes
            {
                Id = x.Id,
                CateTypeName = x.CateTypeName,
                CateTypeCode = x.CateTypeCode
            });
            return Json(listDistrict.ToList());
        }

        private void CheckAvailableCode(Materials Brands)
        {
            Materials NameCheck = unitOfWork.MaterialsRepository.GetOne(x => x.MatCode.Equals(Brands.MatCode));
            if (NameCheck != null)
            {
                ModelState.AddModelError("MatCode", "Mã này đã tồn tại với tên: " + NameCheck.MatName);
            }
        }
    }
}
