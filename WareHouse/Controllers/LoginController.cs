﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using netCoreTutorial.DAL;
using WareHouse.DAL;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly IConfiguration _config;
        private readonly ITokenService _tokenService;
        private string generatedToken = null;
        public LoginController(IUnitOfWork uow, ITokenService tokenService, IConfiguration config)
        {
            unitOfWork = uow;
            _tokenService = tokenService;
            _config = config;
        }



        [HttpPost]
        [IgnoreAntiforgeryToken]
        public IActionResult Logout()
        {

            HttpContext.Session.Clear();

            Unauthorized();
            return RedirectToAction("Signin");

        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Signin()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult Signin(LoginViewModel user)
        {

            if (ModelState.IsValid)
            {

                //Check the user name and password  
                //Here can be implemented checking logic from the database  
                IActionResult response = Unauthorized();
                var listUser = unitOfWork.UsersRepository.GetOne(x => x.Usersname == user.Usersname && x.Password == user.Password && x.isDeactive == false);
                if (listUser != null)
                {
                    generatedToken = _tokenService.BuildToken(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), listUser);
                    if (generatedToken != null)
                    {
                        HttpContext.Session.SetString("Token", generatedToken);
                        string token = HttpContext.Session.GetString("Token");
                        if (token == null)
                        {
                            return (RedirectToAction("Signin"));
                        }
                        if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(), token))
                        {
                            return (RedirectToAction("Signin"));
                        }
                        return RedirectToAction("Index", "Sizes");
                    }
                    else
                    {
                        return RedirectToAction("Signin", new { exception = "wrong-login-information" });
                    }
                    //    //Create the identity for the user  
                    //    var identity = new ClaimsIdentity(new[] {
                    //    new Claim(ClaimTypes.Name, listUser.Name),
                    //    new Claim(ClaimTypes.Role, listUser.Role),
                    //    new Claim(ClaimTypes.NameIdentifier, listUser.Id),
                    //    new Claim(ClaimTypes.GivenName,listUser.Image)
                    //}, CookieAuthenticationDefaults.AuthenticationScheme);

                    //    var principal = new ClaimsPrincipal(identity);

                    //    var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                    //    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return RedirectToAction("Signin", new { exception = "wrong-login-information" });
                }
            }
            else
            {
                return RedirectToAction("Signin");
            }
        }

    }
}
