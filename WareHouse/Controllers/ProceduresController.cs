﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using netCoreTutorial.DAL;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    [Authorize]
    public class ProceduresController : Controller
    {
        // GET: ProceduresController
        private readonly IUnitOfWork unitOfWork;

        public ProceduresController(IUnitOfWork uow)
        {

            unitOfWork = uow;
        }
        public ActionResult Index(string searchKey, int pageNumber = 1, int pageSize = 20)
        {
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.ProceduresRepository.Get(x => !String.IsNullOrEmpty(searchKey) ? (x.ProcedureCode.ToLower().Contains(searchKey.ToLower())
            || x.ProcedureName.ToLower().Contains(searchKey.ToLower())
            ) : true);
            var result = new PagedResult<Procedures>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }

        public ActionResult Create()
        {
            ViewBag.IsCreate = true;
            return View("Form");
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(Procedures procedures)
        {
            ViewBag.IsCreate = true;
            CheckAvailableCode(procedures);
            if (ModelState.IsValid)
            {
                unitOfWork.ProceduresRepository.Add(procedures);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            return View("Form", procedures);
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Procedures procedures = unitOfWork.ProceduresRepository.GetByIdAsync(id);
            if (procedures == null)
            {
                return NotFound();
            }
            ViewBag.Code = procedures.ProcedureCode;
            ViewBag.IsCreate = false;
            return View("Form", procedures);
        }

        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(Procedures procedures, string code)
        {

            if (!code.Equals(procedures.ProcedureCode))
            {
                CheckAvailableCode(procedures);
            }

            if (ModelState.IsValid)
            {

                unitOfWork.ProceduresRepository.Update(procedures);
                await unitOfWork.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Code = code;
            ViewBag.IsCreate = false;
            return View("Form", procedures);
        }

        private void CheckAvailableCode(Procedures procedures)
        {
            Procedures NameCheck = unitOfWork.ProceduresRepository.GetOne(x => x.ProcedureCode.Equals(procedures.ProcedureCode));
            if (NameCheck != null)
            {
                ModelState.AddModelError("ProcedureCode", "Mã này đã tồn tại với tên: " + NameCheck.ProcedureName);
            }
        }
    }
}
