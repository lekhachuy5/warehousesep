﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using netCoreTutorial.DAL;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using WareHouse.Helpers;
using WareHouse.Models;

namespace WareHouse.Controllers
{
    [Authorize]
    public class StatisticsController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private static readonly int[] impo = { Constants.IMPORT, Constants.REIMPORT };

        private DateTime? From;
        private DateTime? To;
        public StatisticsController(IUnitOfWork uow)
        {
            unitOfWork = uow;

        }

        public ActionResult EIS(DateTime? from, DateTime? to, int pageNumber = 1, int pageSize = 20)
        {
            DateTime too = to ?? DateTime.Now;
            DateTime fromm = from ?? new DateTime(too.Year, 1, 1);
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = getReport(fromm, too);
            var tolso = query.Skip(ExcludeRecords).Take(pageSize);
            var result = new PagedResult<ReportEISView>
            {
                Data = tolso.ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }


        public ActionResult Export(DateTime? from, DateTime? to, int pageNumber = 1, int pageSize = 20)
        {
            DateTime too = to ?? DateTime.Now;
            DateTime fromm = from ?? new DateTime(too.Year, 1, 1);
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.ReceiptDetailsRepository.Get(x => (x.CreatedAt >= fromm && x.CreatedAt <= too) && x.Receipt.Type == Constants.EXPORT);
            var result = new PagedResult<ReceiptDetails>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }
        // GET: StatisticsController/Details/5
        public void ThongKeXuat(DateTime? from, DateTime? to)
        {
            DateTime too = to ?? DateTime.Now;
            DateTime fromm = from ?? new DateTime(too.Year, 1, 1);
            var details = unitOfWork.ReceiptDetailsRepository.Get(x => (x.CreatedAt >= fromm && x.CreatedAt <= too) && x.Receipt.Type == Constants.EXPORT);
            ExportExeclEI(details, fromm, too, "Xuất");
        }
        public void ThongKeNhap(DateTime? from, DateTime? to)
        {
            DateTime too = to ?? DateTime.Now;
            DateTime fromm = from ?? new DateTime(too.Year, 1, 1);
            var details = unitOfWork.ReceiptDetailsRepository.Get(x => (x.CreatedAt >= fromm && x.CreatedAt <= too) && impo.Contains(x.Receipt.Type));
            ExportExeclEI(details, fromm, too, "Nhập");
        }

        public void ThongKeXNT(DateTime? from, DateTime? to)
        {
            DateTime too = to ?? DateTime.Now;
            DateTime fromm = from ?? new DateTime(too.Year, 1, 1);
            var details = getReport(fromm,too);
            ExportExeclS(details, fromm, too);
        }
        public ActionResult Import(DateTime? from, DateTime? to, int pageNumber = 1, int pageSize = 20)
        {
            DateTime too = to ?? DateTime.Now;
            DateTime fromm = from ?? new DateTime(too.Year, 1, 1);
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            var query = unitOfWork.ReceiptDetailsRepository.Get(x => (x.CreatedAt >= fromm && x.CreatedAt <= too) && impo.Contains(x.Receipt.Type));
            var result = new PagedResult<ReceiptDetails>
            {
                Data = query.Skip(ExcludeRecords).Take(pageSize).ToList(),
                TotalItems = query.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            return View(result);
        }

        public void ExportExeclEI(IEnumerable<ReceiptDetails> details , DateTime? from, DateTime? to, string name)
        {
            ExcelWorksheet Sheet;
            int row = 4;
            int index = 1;
            DateTime too = to ?? DateTime.Now;
            DateTime fromm = from ?? new DateTime(too.Year, 1, 1);
            this.From = from;
            this.To = to;
            ExcelPackage excelPackage = new ExcelPackage();
            Sheet = excelPackage.Workbook.Worksheets.Add("Thống kê "+name);
            Sheet.Cells["A1:J1"].Merge = true;
            Sheet.Cells["A1:J1"].Value = GenerateHeader();
            Sheet.Cells["A1:J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            Sheet.Cells["A1"].Style.Font.Bold = true;
            Sheet.Cells["A2:A3"].Merge = true;
            Sheet.Cells["B2:C2"].Merge = true;
            Sheet.Cells["D2:D3"].Merge = true;
            Sheet.Cells["E2:E3"].Merge = true;
            Sheet.Cells["F2:F3"].Merge = true;
            Sheet.Cells["G2:G3"].Merge = true;
            Sheet.Cells["H2:J2"].Merge = true;
            Sheet.Cells["A2"].Value = "#";

            Sheet.Cells["B2"].Value = "Chứng từ";
            Sheet.Cells["B3"].Value = "Số hiệu";
            Sheet.Cells["C3"].Value = "Ngày";
            Sheet.Cells["D2"].Value = "Diễn giải";
            Sheet.Cells["E2"].Value = "Mã hàng hóa";
            Sheet.Cells["F2"].Value = "Tên hàng hóa";
            Sheet.Cells["G2"].Value = "Đơn vị";
            Sheet.Cells["H2"].Value = name;
            Sheet.Cells["H3"].Value = "Số lượng";
            Sheet.Cells["I3"].Value = "Đơn giá";
            Sheet.Cells["J3"].Value = "Thành tiền";
            Sheet.Cells["A2:J3"].Style.Font.Bold = true;
            Sheet.Cells["A2:J2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            Sheet.Cells["A3:J3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            Sheet.Cells["A2:J3"].Style.Fill.BackgroundColor.SetColor(Color.DarkRed);
            Sheet.Cells["A2:J3"].Style.Font.Color.SetColor(Color.White);

            foreach (var item in details)
            {
                Sheet.Cells[string.Format("A{0}", row)].Value = index;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.Receipt.LicensesIn;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.CreatedAt;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.Receipt.Description;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.Materials.MatCode;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.Materials.MatName;
                Sheet.Cells[string.Format("G{0}", row)].Value = item.Materials.UnitsMetrics.UnitsName;
                Sheet.Cells[string.Format("H{0}", row)].Value = item.Amount;
                Sheet.Cells[string.Format("I{0}", row)].Value = string.Format("{0:0,0} VNĐ", item.Price);
                Sheet.Cells[string.Format("J{0}", row)].Value = string.Format("{0:0,0} VNĐ", item.TotalPrice);
                row++;
                index++;
            }
            var modelRows = details.Count() + 3;
            string modelRange = "A3:J" + modelRows.ToString();
            var modelTable = Sheet.Cells[modelRange];
            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            modelTable.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            Sheet.Cells["A:AZ"].AutoFitColumns();

            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.Headers["Content-Disposition"] = String.Format("attachment: filename=\"CTT_Thong_ke_{0}.xlsx\"", name);
                excelPackage.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.Body);
            }

        }

        public void ExportExeclS(IEnumerable<ReportEISView> details, DateTime? from, DateTime? to)
        {
            ExcelWorksheet Sheet;
            int row = 4;
            int index = 1;
            DateTime too = to ?? DateTime.Now;
            DateTime fromm = from ?? new DateTime(too.Year, 1, 1);
            this.From = from;
            this.To = to;
            ExcelPackage excelPackage = new ExcelPackage();
            Sheet = excelPackage.Workbook.Worksheets.Add("Thống kê Xuất nhập tồn");
            Sheet.Cells["A1:J1"].Merge = true;
            Sheet.Cells["A1:J1"].Value = GenerateHeader();
            Sheet.Cells["A1:J1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            Sheet.Cells["A1"].Style.Font.Bold = true;
            Sheet.Cells["A2:A3"].Merge = true;
            Sheet.Cells["B2:B3"].Merge = true;
            Sheet.Cells["C2:C3"].Merge = true;
            Sheet.Cells["D2:D3"].Merge = true;
            Sheet.Cells["F2:G2"].Merge = true;
            Sheet.Cells["H2:I2"].Merge = true;
            Sheet.Cells["A2"].Value = "#";

            Sheet.Cells["B2"].Value = "Tên";
            Sheet.Cells["C2"].Value = "Mã";
            Sheet.Cells["D2"].Value = "ĐVT";
            Sheet.Cells["E2"].Value = "Đầu kỳ";
            Sheet.Cells["E3"].Value = "Số lượng";
            Sheet.Cells["F2"].Value = "Nhập kho";
            Sheet.Cells["F3"].Value = "Số lượng";
            Sheet.Cells["G3"].Value = "Giá trị";
            Sheet.Cells["H2"].Value = "Xuất Kho";
            Sheet.Cells["H3"].Value = "Số lượng";
            Sheet.Cells["I3"].Value = "Giá trị";
            Sheet.Cells["J2"].Value = "Cuối kỳ";
            Sheet.Cells["J3"].Value = "Số lượng";
            Sheet.Cells["A2:J3"].Style.Font.Bold = true;
            Sheet.Cells["A2:J2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            Sheet.Cells["A3:J3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            Sheet.Cells["A2:J3"].Style.Fill.BackgroundColor.SetColor(Color.DarkRed);
            Sheet.Cells["A2:J3"].Style.Font.Color.SetColor(Color.White);

            foreach (var item in details)
            {
                Sheet.Cells[string.Format("A{0}", row)].Value = index;
                Sheet.Cells[string.Format("B{0}", row)].Value = item.name;
                Sheet.Cells[string.Format("C{0}", row)].Value = item.code;
                Sheet.Cells[string.Format("D{0}", row)].Value = item.units;
                Sheet.Cells[string.Format("E{0}", row)].Value = item.fiquantity;
                Sheet.Cells[string.Format("F{0}", row)].Value = item.import;
                Sheet.Cells[string.Format("G{0}", row)].Value = string.Format("{0:0,0} VNĐ", item.impPrice);
                Sheet.Cells[string.Format("H{0}", row)].Value = item.export;
                Sheet.Cells[string.Format("I{0}", row)].Value = string.Format("{0:0,0} VNĐ", item.expPrice);
                Sheet.Cells[string.Format("J{0}", row)].Value = item.laquantity;
                row++;
                index++;
            }
            var modelRows = details.Count() + 3;
            string modelRange = "A3:J" + modelRows.ToString();
            var modelTable = Sheet.Cells[modelRange];
            modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            modelTable.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            Sheet.Cells["A:AZ"].AutoFitColumns();

            using (var memoryStream = new MemoryStream())
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.Headers["Content-Disposition"] = String.Format("attachment: filename=\"CTT_Thong_ke_{0}.xlsx\"", "XNT");
                excelPackage.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.Body);
            }

        }


        public IEnumerable<ReportEISView> getReport(DateTime fromm, DateTime too)
        {
            var details = unitOfWork.ReceiptDetailsRepository.Get();
            var materials = unitOfWork.MaterialsRepository.Get();

            var quert =
                from mats in materials
                join detailsmat in details
                on mats.Id equals detailsmat.Materials_Id
                group detailsmat by mats.Id into g
                select new
                {
                    name = g.FirstOrDefault(x => x.Materials_Id == g.Key).Materials.MatName,
                    code = g.FirstOrDefault(x => x.Materials_Id == g.Key).Materials.MatCode,
                    unit = g.FirstOrDefault(x => x.Materials_Id == g.Key).Materials.UnitsMetrics.UnitsName,
                    importquantity = g.Where(x => impo.Contains(x.Receipt.Type) && (x.CreatedAt >= fromm && x.CreatedAt <= too) && x.Materials_Id == g.Key).Select(x => x.Amount).Sum(),
                    importprice = g.Where(x => impo.Contains(x.Receipt.Type) && (x.CreatedAt >= fromm && x.CreatedAt <= too) && x.Materials_Id == g.Key).Select(x => x.TotalPrice).Sum(),
                    exportquantity = g.Where(x => x.Receipt.Type == Constants.EXPORT && (x.CreatedAt >= fromm && x.CreatedAt <= too) && x.Materials_Id == g.Key).Select(x => x.Amount).Sum(),
                    exportprice = g.Where(x => x.Receipt.Type == Constants.EXPORT && (x.CreatedAt >= fromm && x.CreatedAt <= too) && x.Materials_Id == g.Key).Select(x => x.TotalPrice).Sum(),
                    fiQ = g.FirstOrDefault(x => x.Materials_Id == g.Key).Materials.Quantity,
                    exportFiQ = g.Where(x => x.Receipt.Type == Constants.EXPORT && (x.CreatedAt >= fromm && x.CreatedAt <= DateTime.Now) && x.Materials_Id == g.Key).Select(x => x.Amount).Sum(),
                    exportFiP = g.Where(x => x.Receipt.Type == Constants.EXPORT && (x.CreatedAt >= fromm && x.CreatedAt <= DateTime.Now) && x.Materials_Id == g.Key).Select(x => x.TotalPrice).Sum(),
                    impFiQ = g.Where(x => impo.Contains(x.Receipt.Type) && (x.CreatedAt >= fromm && x.CreatedAt <= DateTime.Now) && x.Materials_Id == g.Key).Select(x => x.Amount).Sum(),
                    impFiP = g.Where(x => impo.Contains(x.Receipt.Type) && (x.CreatedAt >= fromm && x.CreatedAt <= DateTime.Now) && x.Materials_Id == g.Key).Select(x => x.TotalPrice).Sum(),
                    fiP = g.FirstOrDefault(x => x.Materials_Id == g.Key).Materials.SellPrice,
                    laQ = g.Where(x => x.Materials_Id == g.Key && x.CreatedAt <= fromm).Select(x => x.Amount).Sum(),
                    laP = g.Where(x => x.Materials_Id == g.Key && x.CreatedAt <= too).Select(x => x.Price).Sum(),
                };
            var query = quert.Select(x => new ReportEISView
            {
                name = x.name,
                code = x.code,
                units = x.unit,
                fiquantity = (Convert.ToInt32(x.fiQ) - x.impFiQ + x.exportFiQ),
                fistorage = x.fiP * (Convert.ToInt32(x.fiQ) - x.impFiQ + x.exportFiQ),
                import = x.importquantity,
                impPrice = x.importprice,
                export = x.exportquantity,
                expPrice = x.exportprice,
                laquantity = (Convert.ToInt32(x.fiQ) - x.impFiQ + x.exportFiQ) + x.importquantity - x.exportquantity,
                lastorage = (x.fiP * (Convert.ToInt32(x.fiQ) - x.impFiQ + x.exportFiQ)) + x.importprice - x.exportprice,
            });
            return query;
        }
        string GenerateHeader()
        {
            string header = "Thống kê từ ngày: " + (this.From.HasValue ? From.Value.ToString("dd-MM-yyyy") : " --/--/----") + " đến ngày: " + (this.To.HasValue ? To.Value.ToString("dd-MM-yyyy") : " --/--/----");
            return header;
        }
    }
}
