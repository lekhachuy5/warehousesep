﻿namespace WareHouse.Helpers
{
    public class FileConstants
    {
        public static readonly string UpLoadPath = "Storages/Uploads/";
        public static readonly string TemplatePath = "~/Storages/Templates/";
        public static readonly string ProcessPath = "~/Storages/Processes/";
        public static readonly string OutputPath = "~/Storages/Outputs/";
        public static readonly string LogoPath = "~/Storages/Logos/";
        public static readonly string OfflinesPath = "~/Storages/Offlines/";

        public static readonly string[] AllowExtension = {".doc", ".docx", ".pdf", ".xlsx", ".xls", ".jpg", ".jpeg", ".png"};

        public static readonly string UploadName = "fileUploads";
    }
}