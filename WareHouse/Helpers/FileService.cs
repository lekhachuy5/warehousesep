﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WareHouse.Helpers
{
    public class FileService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileBases"></param>
        /// <param name="path"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public string UploadFile(List<IFormFile> fileBases, string path, string prefix)
        {
            string fileNames = "";
            if (fileBases != null && fileBases.Count() > 0)
            {
                foreach (var file in fileBases)
                {
                    if (file != null)
                    {
                        string extension = Path.GetExtension(file.FileName);
                        if (FileConstants.AllowExtension.Contains(extension.ToLower()))
                        {
                            string fileName = GenerateFileName(Path.GetFileName(file.FileName), prefix);
                            string physicalPath = Path.Combine(path, fileName);
                            using (var stream = System.IO.File.Create(physicalPath))
                            {
                                // The files are not actually saved in this demo
                                file.CopyToAsync(stream);
                            }
                            fileNames += String.IsNullOrEmpty(fileNames) ? fileName : ";" + fileName;
                        }
                    }
                }
            }
            return fileNames;
        }

        /// <summary>
        /// DeleteFile
        /// </summary>
        /// <param name="FilePath"></param>
        public void DeleteFile(string FilePath)
        {
            if (File.Exists(FilePath))
            {
                File.Delete(FilePath);
            }
        }

        /// <summary>
        /// CreateDirectory
        /// </summary>
        /// <param name="directoryName"></param>
        public void CreateDirectory(string directoryName)
        {
            string path = Path.Combine(FileConstants.UpLoadPath,directoryName);
            //kiem tra da co folder cho form do hay chua
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

      
        /// <summary>
        /// GenerateFileName
        /// </summary>
        /// <param name="originalname"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public string GenerateFileName(string originalname, string prefix)
        {
            string newName = prefix + "_" + RandomFileNamePrefix() + "_" + originalname;
            return newName;
        }

        /// <summary>
        /// RandomFileNamePrefix
        /// </summary>
        /// <returns></returns>
        private string RandomFileNamePrefix()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4, true));
            builder.Append(RandomNumber(1000, 9999));
            builder.Append(RandomString(2, false));
            return builder.ToString();
        }

        /// <summary>
        /// RandomString
        /// </summary>
        /// <param name="size"></param>
        /// <param name="lowerCase"></param>
        /// <returns></returns>
        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        /// <summary>
        /// RandomNumber
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
    }
}