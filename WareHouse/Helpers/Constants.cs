﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WareHouse.Helpers
{
    public class Constants
    {
        public static int IMPORT = 1;
        public static int REIMPORT = 2;
        public static int EXPORT = 3;
        public static string SUBDOMAINS = "/SEP23Team10";
        public static string ADMIN = "ADMIN";
        public static string USER = "User";
        public const string ADMINS = "ADMIN";
        public const string USERS = "User";
        public static readonly Dictionary<int, string> CODESWITCH = new Dictionary<int, string> {
            {IMPORT, "NM"},
            {REIMPORT, "NT"},
            {EXPORT, "XK"}
        };

        public static readonly Dictionary<int, string> TITLESWITCH = new Dictionary<int, string> {
            {IMPORT, "NHẬP MỚI"},
            {REIMPORT, "NHẬP TRẢ"},
            {EXPORT, "XUẤT KHO"}
        };
    }
}
