﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace WareHouse.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
               name: "Users",
               columns: table => new
               {
                   Id = table.Column<string>(nullable: false, maxLength: 36),
                   Usersname = table.Column<string>(nullable: false),
                   Password = table.Column<string>(nullable: false),
                   Name = table.Column<string>(nullable: false),
                   Address = table.Column<string>(nullable: true),
                   Email = table.Column<string>(nullable: true),
                   Phone = table.Column<string>(nullable: false),
                   isDeactive = table.Column<bool>( nullable: true, defaultValue: false),
                   Image = table.Column<string>(nullable: true),
                   Role = table.Column<string>(nullable:true)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_users", x => x.Id);
               });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
               name: "Users");
        }
    }
}
