﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace WareHouse.Migrations
{
    public partial class material : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                 name: "materials",
                 columns: table => new
                 {
                     Id = table.Column<int>(nullable: false)
                         .Annotation("SqlServer:Identity", "1, 1"),
                     MatCode = table.Column<string>(maxLength: 12, nullable: false),
                     MatName = table.Column<string>(nullable: false),
                     Note = table.Column<string>(nullable:true),
                     Locations = table.Column<string>(nullable:false),
                     Units = table.Column<int>(nullable:true),
                     SizesMat = table.Column<int>(nullable: true),
                     BrandsMat = table.Column<int>(nullable: true),
                     ColorsMat = table.Column<int>(nullable: true),
                     Images = table.Column<string>(nullable: true),
                     Descriptions = table.Column<string>(nullable: true),
                     GruopMat = table.Column<int>(nullable: true),
                     TypeMat = table.Column<int>(nullable: true),
                     PrimePrice = table.Column<double>(nullable:false),
                     SellPrice = table.Column<double>(nullable: false),
                     CreatedAt = table.Column<DateTime>(nullable:false),
                     UpdateAt = table.Column<DateTime>(nullable: false),
                     Quantity = table.Column<long>(nullable:false),
                     Warranty = table.Column<int>(nullable: false),
                     Duration = table.Column<int>(nullable: false),
                 },
                 constraints: table =>
                 {
                     table.PrimaryKey("PK_materials", x => x.Id);
                     table.ForeignKey("FK_Mat_Units", x => x.Units, "unitsmetrics", "Id");
                     table.ForeignKey("FK_Mat_Sizes", x => x.SizesMat, "sizes", "Id");
                     table.ForeignKey("FK_Mat_Brands", x => x.BrandsMat, "brands", "Id");
                     table.ForeignKey("FK_Mat_Colors", x => x.ColorsMat, "colors", "Id");
                     table.ForeignKey("FK_Mat_Groups", x => x.GruopMat, "categorygroups", "Id");
                     table.ForeignKey("FK_Mat_Types", x => x.TypeMat, "categorytypes", "Id");
                 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
               name: "materials");
        }
    }
}
