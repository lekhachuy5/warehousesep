﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WareHouse.Migrations
{
    public partial class ReceiptTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.CreateTable(
                name: "Receipts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerCode = table.Column<string>(maxLength: 20, nullable: false),
                    CustomerName = table.Column<string>(nullable: false),
                    ReceiptCode = table.Column<string>(maxLength: 20, nullable: false),
                    CompanyName = table.Column<string>(nullable: true),
                    LicensesIn = table.Column<string>(nullable: true),
                    LicensesCheck = table.Column<string>(nullable: true),
                    Receiver = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: false),
                    Adress = table.Column<string>(nullable: false),
                    ReceiveDay = table.Column<DateTime>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdateAt = table.Column<DateTime>(nullable: false),
                    TotalPrice = table.Column<double>(nullable:false),
                    Description = table.Column<string>(nullable:true),
                    Note = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: true),
                    IsEdit = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Receipt", x => x.Id);

                });

        }
        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropTable(
                name: "Receipts");


        }
    }
}