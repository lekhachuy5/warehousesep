﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WareHouse.Migrations
{
    public partial class UnitsMetric : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                   name: "unitsmetrics",
                   columns: table => new
                   {
                       Id = table.Column<int>(nullable: false).Annotation("SqlServer:Identity", "1, 1"),
                       UnitsCode = table.Column<string>(nullable: false, maxLength: 2),
                       UnitsName = table.Column<string>(nullable: false)
                   },
                   constraints: table =>
                   {
                       table.PrimaryKey("PK_unitsmetric", x => x.Id);
                   });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
             name: "unitsmetrics");
        }
    }
}
