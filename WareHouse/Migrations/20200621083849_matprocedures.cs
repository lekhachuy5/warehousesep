﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WareHouse.Migrations
{
    public partial class matprocedures : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                name: "mats_proces",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false).Annotation("SqlServer:Identity", "1, 1"),
                    MatId = table.Column<int>(nullable: false),
                    ProceId = table.Column<int>(nullable: false),
                    LevelProce = table.Column<int>(nullable:true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mats_proces", x => x.Id);
                    table.ForeignKey("FK_matproc_Mat", x => x.MatId, "materials", "Id");
                    table.ForeignKey("FK_matproc_Pro", x => x.ProceId, "procedures", "Id");
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
               name: "mats_proces");

        }
    }
}
