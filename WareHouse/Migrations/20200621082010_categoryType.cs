﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WareHouse.Migrations
{
    public partial class categoryType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                            name: "categorytypes",
                            columns: table => new
                            {
                                Id = table.Column<int>(nullable: false)
                                    .Annotation("SqlServer:Identity", "1, 1"),
                                CateTypeCode = table.Column<string>(maxLength: 2, nullable: false),
                                CateTypeName = table.Column<string>(nullable: false),
                                CateLocation = table.Column<int>(nullable: true),
                                Group_Id = table.Column<int>(nullable:true)
                            },
                            constraints: table =>
                            {
                                table.PrimaryKey("PK_categorytypes", x => x.Id);
                                table.ForeignKey("FK_CateType_CateGroup", x => x.Group_Id, "categorygroups", "Id");
                            });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "categorytypes");
        }
    }
}
