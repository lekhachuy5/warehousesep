﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace WareHouse.Migrations
{
    public partial class ReceiptDeatals : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                name: "ReceiptDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Receipt_Id = table.Column<long>(nullable:false),
                    Materials_Id = table.Column<int>(nullable: false),
                    Duration = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Procedure_Materials_Id = table.Column<int>(nullable:false),
                    Quantity = table.Column<int>(nullable:false),
                    Price = table.Column<double>(nullable:false),
                    TotalPrice = table.Column<double>(nullable:false),
                    Amount = table.Column<int>(nullable:false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceiptDetails", x => x.Id);
                    table.ForeignKey("FK_Receipt_Details", x => x.Receipt_Id, "Receipts", "Id");
                    table.ForeignKey("FK_Material_Receipt", x => x.Materials_Id, "materials", "Id");
                    table.ForeignKey("FK_Pro_Details", x => x.Procedure_Materials_Id, "mats_proces", "Id");
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
               name: "ReceiptDetails");
        }
    }
}
