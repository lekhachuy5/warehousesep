﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WareHouse.Migrations
{
    public partial class Brand : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                  name: "brands",
                  columns: table => new
                  {
                      Id = table.Column<int>(nullable: false).Annotation("SqlServer:Identity", "1, 1"),
                      BrandCode = table.Column<string>(nullable: false, maxLength: 2),
                      BrandName = table.Column<string>(nullable: false)
                  },
                  constraints: table =>
                  {
                      table.PrimaryKey("PK_brands", x => x.Id);
                  });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
             name: "brands");
        }
    }
}
