USE [master]
GO
/****** Object:  Database [SEP23Team10]    Script Date: 25/02/2022 10:54:59 PM ******/
CREATE DATABASE [SEP23Team10]

USE [SEP23Team10]
GO
/****** Object:  User [SEP23Team10]    Script Date: 25/02/2022 10:55:00 PM ******/
CREATE USER [SEP23Team10] FOR LOGIN [SEP23Team10] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [SEP23Team10]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[brands]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[brands](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BrandCode] [nvarchar](2) NOT NULL,
	[BrandName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_brands] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[categorygroups]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[categorygroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CateGroupCode] [nvarchar](2) NOT NULL,
	[CateGroupName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_categorygroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[categorytypes]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[categorytypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CateTypeCode] [nvarchar](2) NOT NULL,
	[CateTypeName] [nvarchar](max) NOT NULL,
	[CateLocation] [int] NULL,
	[Group_Id] [int] NULL,
 CONSTRAINT [PK_categorytypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[colors]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[colors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ColorCode] [nvarchar](2) NOT NULL,
	[ColorName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_colors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[materials]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[materials](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MatCode] [nvarchar](12) NOT NULL,
	[MatName] [nvarchar](max) NOT NULL,
	[Note] [nvarchar](max) NULL,
	[Locations] [nvarchar](max) NOT NULL,
	[Units] [int] NULL,
	[SizesMat] [int] NULL,
	[BrandsMat] [int] NULL,
	[ColorsMat] [int] NULL,
	[Images] [nvarchar](max) NULL,
	[Descriptions] [nvarchar](max) NULL,
	[GruopMat] [int] NULL,
	[TypeMat] [int] NULL,
	[PrimePrice] [float] NOT NULL,
	[SellPrice] [float] NOT NULL,
	[CreatedAt] [datetime2](7) NOT NULL,
	[UpdateAt] [datetime2](7) NOT NULL,
	[Quantity] [bigint] NOT NULL,
	[Warranty] [int] NOT NULL,
	[Duration] [int] NOT NULL,
 CONSTRAINT [PK_materials] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mats_proces]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mats_proces](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MatId] [int] NOT NULL,
	[ProceId] [int] NOT NULL,
	[LevelProce] [int] NULL,
 CONSTRAINT [PK_mats_proces] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[procedures]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[procedures](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProcedureCode] [nvarchar](max) NULL,
	[ProcedureName] [nvarchar](max) NULL,
	[Quantity] [int] NULL,
	[ParentCode] [nvarchar](max) NULL,
 CONSTRAINT [PK_procedures] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReceiptDetails]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReceiptDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Receipt_Id] [bigint] NOT NULL,
	[Materials_Id] [int] NOT NULL,
	[Duration] [int] NOT NULL,
	[CreatedAt] [datetime2](7) NOT NULL,
	[Procedure_Materials_Id] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Price] [float] NOT NULL,
	[TotalPrice] [float] NOT NULL,
	[Amount] [int] NOT NULL,
 CONSTRAINT [PK_ReceiptDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Receipts]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Receipts](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerCode] [nvarchar](20) NOT NULL,
	[CustomerName] [nvarchar](max) NOT NULL,
	[ReceiptCode] [nvarchar](20) NOT NULL,
	[CompanyName] [nvarchar](max) NULL,
	[LicensesIn] [nvarchar](max) NULL,
	[LicensesCheck] [nvarchar](max) NULL,
	[Receiver] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[Phone] [nvarchar](max) NOT NULL,
	[Adress] [nvarchar](max) NOT NULL,
	[ReceiveDay] [datetime2](7) NOT NULL,
	[CreatedAt] [datetime2](7) NOT NULL,
	[UpdateAt] [datetime2](7) NOT NULL,
	[TotalPrice] [float] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[Type] [int] NULL,
	[IsEdit] [bit] NULL,
 CONSTRAINT [PK_Receipt] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sizes]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sizes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SizeCode] [nvarchar](2) NOT NULL,
	[SizeName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_size] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[unitsmetrics]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[unitsmetrics](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UnitsCode] [nvarchar](2) NOT NULL,
	[UnitsName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_unitsmetric] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 25/02/2022 10:55:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [nvarchar](36) NOT NULL,
	[Usersname] [nvarchar](max) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Address] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NOT NULL,
	[isDeactive] [bit] NULL,
	[Image] [nvarchar](max) NULL,
	[Role] [nvarchar](50) NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200612024955_Initial', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200615013421_ProceduresTable', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200615013839_Color', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200615014241_Size', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200615014507_Company', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200615014618_Customers', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200615112011_Brand', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200615112138_UnitsMetric', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200621081812_categoryGroup', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200621082010_categoryType', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200621082509_material', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200621083849_matprocedures', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200706002321_ReceiptTable', N'3.1.5')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200706003054_ReceiptDeatals', N'3.1.5')
GO
SET IDENTITY_INSERT [dbo].[brands] ON 

INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (1, N'S0', N'Sunco')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (2, N'L2', N'Tiến thành')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (3, N'CD', N'Cadivi')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (4, N'EL', N'Elora')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (5, N'MC', N'Michelin')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (6, N'GR', N'GoRacing')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (7, N'GL', N'Globe')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (8, N'LG', N'LG')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (9, N'SS', N'Samsung')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (10, N'Y0', N'Yamaha')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (11, N'SN', N'Sony')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (12, N'MT', N'Motul')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (13, N'AP', N'Apple')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (14, N'MS', N'Microsoft')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (15, N'DE', N'Dell')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (16, N'OM', N'Omachi')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (17, N'DO', N'Downy')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (18, N'VO', N'Voltronic')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (19, N'WE', N'Wella')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (20, N'TO', N'Toshiba')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (21, N'AS', N'Asus')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (22, N'PH', N'Philips')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (23, N'DQ', N'Điện Quang')
INSERT [dbo].[brands] ([Id], [BrandCode], [BrandName]) VALUES (24, N'RI', N'Rizoma')
SET IDENTITY_INSERT [dbo].[brands] OFF
GO
SET IDENTITY_INSERT [dbo].[categorygroups] ON 

INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (1, N'o1', N'Ốc vít')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (2, N'Đ1', N'Đinh')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (3, N'DĐ', N'Dây điện ')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (4, N'TV', N'Tua vít')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (5, N'VX', N'Vỏ xe')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (6, N'BT', N'Chai xịt bôi trơn sên')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (7, N'AQ', N'Bình ắc quy')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (8, N'TI', N'Ti-vi')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (9, N'ĐT', N'Điện thoại ')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (10, N'TP', N'Thực Phẩm')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (13, N'LN', N'Loa nghe nhạc')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (14, N'XM', N'Xe máy')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (16, N'GD', N'Đồ Gia Dụng')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (17, N'LK', N'Linh kiện máy tính')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (18, N'NX', N'Nhớt xe máy')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (19, N'DX', N'Đồ chơi xe máy')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (20, N'PT', N'Phương tiện đi lại')
INSERT [dbo].[categorygroups] ([Id], [CateGroupCode], [CateGroupName]) VALUES (21, N'LA', N'Laptop')
SET IDENTITY_INSERT [dbo].[categorygroups] OFF
GO
SET IDENTITY_INSERT [dbo].[categorytypes] ON 

INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (7, N'TC', N'Ốc vít 3mm', 1, 1)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (8, N'TS', N'Đinh 5mm', 1, 2)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (9, N'L2', N'Ốc vít 2mm', 2, 1)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (10, N'L4', N'Đinh 2mm', 2, 2)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (11, N'DT', N'Dây điện Cadivi ', 1, 3)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (12, N'T4', N'Tua vít dẹp', 3, 4)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (13, N'VM', N'Vỏ Michelin', 1, 5)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (14, N'BG', N'Chai xịt bôi trơn sên GoRacing', 1, 6)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (15, N'AG', N'Bình ắc quy Globe ', 1, 7)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (16, N'ST', N'Smart Tivi LG ', 1, 8)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (17, N'SM', N'Điện thoại Samsung ', 3, 9)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (18, N'TV', N'TV65inch', 2, 8)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (19, N'LB', N'Loa Bluetooth', 1, 13)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (20, N'ND', N'Nồi Cơm Điện', 1, 16)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (21, N'NK', N'Nhớt Khoáng', 1, 18)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (22, N'NB', N'Nhớt bán tổng hợp', 2, 18)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (23, N'NT', N'Nhớt tổng hợp', 3, 18)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (24, N'GC', N'Gương chiếu hậu', 3, 19)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (25, N'TH', N'Tay thắng', 2, 19)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (26, N'HT', N'Heo Thắng', 3, 19)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (27, N'XM', N'Xe máy', 3, 20)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (28, N'MG', N'Mì gói ', 1, 10)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (29, N'DD', N'Bóng đèn điện', 2, 16)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (30, N'MB', N'Mainboard', 1, 17)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (31, N'VG', N'Card Màn Hình', 2, 17)
INSERT [dbo].[categorytypes] ([Id], [CateTypeCode], [CateTypeName], [CateLocation], [Group_Id]) VALUES (32, N'LP', N'Laptop', 1, 21)
SET IDENTITY_INSERT [dbo].[categorytypes] OFF
GO
SET IDENTITY_INSERT [dbo].[colors] ON 

INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (1, N'VR', N'Nâu')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (2, N'G2', N'Xám')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (3, N'V0', N'Đỏ')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (4, N'VY', N'Vàng')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (5, N'B1', N'Đen')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (6, N'C2', N'Cam')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (7, N'WH', N'Trắng')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (8, N'PP', N'Tím')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (9, N'XD', N'Xanh Dương')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (10, N'GR', N'Xanh Lá')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (11, N'PK', N'Hồng')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (12, N'CO', N'Đồng')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (13, N'VC', N'Vàng Chanh')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (14, N'SR', N'Bạc')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (15, N'LB', N'Lục Bảo')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (16, N'CH', N'Chàm')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (17, N'TI', N'Tía')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (18, N'DT', N'Xanh da trời')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (19, N'TX', N'Trắng Xanh')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (20, N'YS', N'Vàng Nắng')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (21, N'BR', N'Nâu Đỏ')
INSERT [dbo].[colors] ([Id], [ColorCode], [ColorName]) VALUES (22, N'C1', N'Vàng')
SET IDENTITY_INSERT [dbo].[colors] OFF
GO
SET IDENTITY_INSERT [dbo].[materials] ON 

INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (5, N'L2XLLOVR2', N'Ốc vít m1', NULL, N'Việt nam', 1, 1, 1, 1, N'logo-inverse205417381.png', N'Ốc vít m.1 giá rẻ5 6 62 2', 1, 9, 50000, 53750, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2020-08-07T21:05:37.7574987' AS DateTime2), 120724, 10, 10)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (6, N'L4XLL2VR2', N'Đinh tán', NULL, N'Việt nam', 2, 1, 2, 1, N'DINHTANR205606049.jpg', N'Đinh tán 30', 2, 10, 20000, 25000, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2020-07-08T08:14:39.7208582' AS DateTime2), 4177, 20, 20)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (8, N'TCSMS0VR01', N'123', NULL, N'1231', 1, 1, 1, 1, N'logo-inverse200243693.png', N'213', 1, 7, 0, 0, CAST(N'2020-06-28T12:02:43.6931545' AS DateTime2), CAST(N'2020-06-28T12:02:43.6929283' AS DateTime2), 0, 0, 0)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (10, N'L2SMS0VR02', N'123', N'123', N'Việt nam', 1, 1, 1, 1, N'logo-inverse202415737.png', N'123', 1, 9, 1, 2, CAST(N'2020-07-17T10:24:15.7373296' AS DateTime2), CAST(N'2020-07-17T10:24:15.7370687' AS DateTime2), 0, 1, 1)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (11, N'TCXLTNVY01', N'Dây điện Cadivi VCmd 2x2.5 - 0,6/1kV', NULL, N'Việt Nam', 1, 2, 3, 4, N'cover200709648.jpg', N'Dây điện Cadivi VCmd 2x2.5 - 0,6/1kV là sản phẩm thuộc cáp điện hạ thế CV. Sản phẩm có cấu tạo gồm 1 lớp vỏ bên ngoài được làm bằng nhựa PVC cao cấp có khả năng cách điện tốt, tạo được độ an toàn khi dẫn điện trong quá trình sử dụng.', 3, 11, 990000, 1300000, CAST(N'2020-08-08T08:43:51.0000000' AS DateTime2), CAST(N'2020-08-08T12:07:09.6474939' AS DateTime2), 27, 6, 24)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (12, N'TCSHELVY01', N'Tua vít thợ điện 2 cạnh ELORA 649-IS-100', NULL, N'Việt Nam', 1, 4, 4, 4, N'to-vit-tho-dien-2-canh-elora-649-is-100205607816.jpg', N'Tuốc nơ vít thợ điện 100mm, Tô vít thợ điện 2 cạnh.

Tuốc nơ vít thợ điện dẹt 649-IS 100 có thân thân tròn, thẳng, lưỡi dẹt.
Chuyên dụng cho thợ điện, kỹ sư điện hệ thống, kỹ sư điện tự động.
Đặc điểm nổi bật là thân tròn, đầu tuốc nơ vít song song suốt thân.
Bề rộng lưỡi bằng đường kính thân, thuận tiện cho các thao tác trong lỗ phần tử tự động.
Tô vít dẹt hay tô vít đầu “-” có lưỡi mảnh 0,6mm, phủ phosphat đen.
Thân tuốc nơ vít mạ Chrome. Độ rộng lưỡi chỉ 3,5mm.
Cán tô vít bằng nhựa cao cấp, mềm, cho cảm giác cầm êm tay.
Tay cầm bằng nhựa 2 màu Vàng – Đen tương phản. Đúc theo khuôn nắm tay.
Công nghệ bản quyền chống trượt QUATROLIT®, 2C Handle.
Chuôi tuốc nơ vít có lỗ treo dụng cụ.
Tô vít theo công nghệ bản quyền: ELORA-Chrome-Vanadium 59CrV4 / 1.2242.
Đáp ứng tiêu chuẩn DIN ISO 2380-2.
Lưỡi tuốc nơ vít đáp ứng tiêu chuẩn: DIN ISO 2380-1.', 4, 12, 20000, 30000, CAST(N'2020-08-08T11:42:04.0000000' AS DateTime2), CAST(N'2020-08-08T11:56:07.8152337' AS DateTime2), 32, 12, 24)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (13, N'TCXLMCB101', N'Vỏ Michelin Pilot Street 2 150/60-17', NULL, N'Thái Lan', 1, 2, 5, 5, N'vo-michelin-pilot-street-2-15060-17-1291-slide-products-5f27e1531bef0205120076.jpeg', N'Vỏ Michelin Pilot Street 2 hoàn toàn mới được thiết kế sử dụng hàng ngày, có tuổi thọ cao nhờ có kết cấu chắc chắn. Gai vỏ rất sâu có rảnh dày từ trong ra ngoài nên có khả năng bám tốt trên đường đô thị kể cả đường trơn trượt giúp cho hành trình lái xe và người lái được an toàn.
Vỏ Michelin Pilot Street 2 có quãng đường đi trung bình vượt trội trên 20.000 km. Đảm bảo độ bám đường tuyệt vời  ngay cả trên bề mặt đường trơn ướt nhờ thiết kế mặt gai mới, cấu trúc lốp và thiết kế rãnh.
Vỏ Michelin 150/60-17 Pilot Street 2 gắn được các dòng xe moto tầm dưới 300cc như: Ninja 300, Z300, R3, CBR250, CBR150, R15, GSX150...
Vỏ Michelin Pilot Street 2 hoàn toàn mới được sản xuất tại Thái Lan.', 5, 13, 1800000, 1835000, CAST(N'2020-08-08T11:51:20.0766644' AS DateTime2), CAST(N'2020-08-08T11:51:20.0766510' AS DateTime2), 10, 12, 36)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (14, N'TCXLGRVY01', N'Chai xịt bôi trơn sên GoRacing', NULL, N'Việt Nam', 1, 2, 6, 4, N'chai-xit-sen-goracing-848-slide-products-5baca82c1ba30201635995.jpg', N'Chai xịt bôi trơn sên GoRacing (Chain Lube) là hỗn hợp dầu bôi trơn, chất hòa tan, khí hóa lỏng. Giúp tạo một lớp phủ lên bề mặt sên, giảm tiếng ồn, chống rỉ sét tận bên trong lõi sên, hạn chế bám bụi, giữ sạch sên lâu hơn.

Chai bôi trơn sên GoRacing với 3 tính năng chính:
- Bôi Trơn Lâu: Dung dịch có khả năng bám tốt trên sên từ 7 đến 10 ngày, ít văng ra ngoài khi vận hành.
- Sên Chạy Êm: Khả năng bôi trơn tốt giúp giảm tiếng kêu khi sên khô, làm xe chạy êm, nhanh hơn.
- Ít Bám Bụi: Bảo vệ sên tốt, ít bám cát, đất khi vận hành, dễ dàng vệ sinh sau thời gian sử dụng.', 6, 14, 100000, 150000, CAST(N'2020-08-08T12:16:35.9958403' AS DateTime2), CAST(N'2020-08-08T12:16:35.9958320' AS DateTime2), 102, 12, 24)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (15, N'DTMEGLB101', N'Bình ắc quy Globe WTZ5S-E', NULL, N'Việt Nam', 1, 5, 7, 5, N'binh-ac-quy-globe-wtz5s-e-561-slide-products-58e4aee95255d202423840.jpg', N'Loại Bình: GLOBE WTZ5S-E 
Điện thế: 12 V
Dung Lượng: 4 AH
Dài: 113 mm
Rộng: 70 mm
Cao: 86 mm
Tổng cao: 86 mm', 7, 15, 200000, 270000, CAST(N'2020-08-08T12:24:23.8404193' AS DateTime2), CAST(N'2020-08-08T12:24:23.8404126' AS DateTime2), 120, 6, 36)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (16, N'TCXLLGB101', N'Smart Tivi LG 4K 65 inch 65SM8100PTA', NULL, N'Hàn Quốc', 1, 2, 8, 5, N'unnamed203326154.jpg', N'Smart Tivi LG 4K 65 inch 65SM8100PTA có thiết kế siêu mỏng hiện đại, viền mỏng cho kích thước màn hình rộng hơn. Chân đế có kiểu dáng uốn cong sang trọng, kích thước màn hình lớn thích hợp trang trí tạo điểm nhấn cho phòng khách, phòng hội nghị, phòng họp,...

Màu sắc nổi bật, tinh tế với công nghệ NanoCell
Đây là dòng TV LED tân tiến nhất của hãng LG, mang đến những chất lượng hình ảnh chuẩn sắc nét cùng với độ thuần khiết màu RGB nâng cao nhờ công nghệ NanoCell của LG. Những hình ảnh được truyền tải tuyệt đẹp, sống động với công nghê này, độ màu lên chuẩn và thuần khiết nhờ các hạt nano 1nm.. Các hạt nano giúp lọc màu bằng cách lọc bỏ các màu xỉn và nâng cao độ thuần khiết của phổ RGB.

Với Smart Tivi LG 4K 65 inch 65SM8100PTA thì dù bạn ngồi ở bất kỳ góc nào cũng sẽ duy trì màu sắc chính xác, cho người dùng những trải nghiệm chân thực nhất. Đây chính là điểm khác biệt với các loại TV thường thấy, màu sẽ biến dạng khi xem từ các góc hẹp bên cạnh.

Cảm nhận độ tương phản mạnh với công nghệ LED nền, Tivi có màu đen sau giúp gia tăng độ sâu cho tất cả các màu. Điều này giúp kiểm soát chính xác các đơn vị chiếu sáng tạo ra hình ảnh chi tiết khi có màu đen sâu hơn.

Không gian sống động hơn với 4K Cinema HDR 
LG 4K 65 inch 65SM8100PTA sở hữu công nghệ xem phim 4K Cinema HDR của LG hỗ trợ hầu hết các định dạng HDR. Bạn sẽ được thưởng thức những thước phim điện ảnh siêu nét cùng các định dạng HDR chính bao gồm Advanced HDR của Techncolor và HDR10 Pro.

Với công nghệ này bạn sẽ được trải nghiệm hình ảnh điện ảnh hoàn hảo khi chơi game 4K HDR Gaming. Tivi sẽ thể hiện hình ảnh với độ lag thấp, giúp bạn như hòa mình với khung ảnh theo thời gian thực và tận hưởng các phân cảnh hành động trong trò chơi.

Trải nghiệm âm thanh vòm DTS Virtual:X
Tích hợp công nghệ Dolby LG 4K 65 inch 65SM8100PTA mang đến âm thanh từ rạp phim đến với gia đình bạn. Hơn thế nữa, Tivi còn trang bị chip xử lý Quad Core nhanh và chính xác loại bỏ nhiễu và tạo ra màu sắc và độ tương phản sinh động hơn.

Hệ điều hành WebOS đơn giản dễ sử dụng
Smart Tivi LG 4K 65 inch 65UM7600PTA sử dụng hệ điều hành WebOS nên sở hữu nhiều ứng dụng giải trí trực tuyến phổ biến: Trình duyệt web, YouTube, Netflix, Zing TV, VnExpress, Film+, The Karaoke Chanel, mang đến không gian giải trí phong phú cho người dùng.

Hỗ trợ nhiều cổng kết nối đa dạng
– HDMI: Kết nối tivi với laptop, pc, dàn âm thanh để trình chiếu các nội dung hình ảnh, phim, ca nhạc.
– USB: Kết nối và phát trực tiếp nội dung có sẵn trên USB vô cùng hữu ích
– Kết nối mạng LAN, WIFI để kết nối Internet giúp bạn dễ dàng xem xem Youtube, truy cập Facebook, đọc báo', 8, 16, 23990000, 42900000, CAST(N'2020-08-08T12:33:26.1541641' AS DateTime2), CAST(N'2020-08-08T12:33:26.1541570' AS DateTime2), 90, 24, 60)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (17, N'TCMESSB101', N'Điện thoại Samsung Galaxy A30s', NULL, N'Hàn Quốc', 1, 5, 9, 5, N'samsung-galaxy-a30s-1-1205212675.jpg', N'Samsung Galaxy A30s, chiếc smartphone mới ra mắt sở hữu nhiều ưu điểm nổi bật trong phân khúc, nổi bật nhất phải kể đến là dung lượng pin lên tới 4000 mAh, bộ 3 camera cùng vi xử lý đủ mạnh, ổn định.

Nâng cấp mạnh mẽ về camera
Thay vì sử dụng camera kép như trên người anh em Samsung Galaxy A30 thì Samsung đã nâng cấp cho chiếc Galaxy A30s bộ 3 camera chất lượng ở mặt lưng.
Máy sở hữu ống kính chính sẽ có độ phân giải 25 MP, ống kính thứ 2 là ống kính góc siêu rộng có độ phân giải 8 MP và một ống kính 5 MP hỗ trợ đo độ sâu trường ảnh.
Ngoài ra, những tính năng chụp ảnh xóa phông, tự động lấy nét, HDR, Panorama,... sẽ đáp ứng được nhu cầu nắm bắt khoảnh khắc hàng ngày của bạn và chia sẻ với bạn bè.
Tính năng Live Focus trên Galaxy A30s cho khả năng xóa phông thời thượng bằng cách nhận diện khuôn mặt và làm mờ phần phông nền phía sau.
Trong khi đó camera selfie có độ phân giải lên tới 16 MP và tất nhiên vẫn hỗ trợ nhiều tính năng hiện đại như lấy nét tự động, chụp ảnh xóa phông, làm mịn da…
Với camera này thì các bạn trẻ có thể tự tin khoe hình tự sướng của mình ngay sau khi chụp lên facebook mà không cần phải chỉnh sửa gì thêm.

Thời lượng pin là điểm sáng
Samsung Galaxy A30s được trang bị viên pin lên tới 4000 mAh, một con số khá ấn tượng trên smartphone tầm trung ở thời điểm hiện tại.
Bạn hoàn toàn có thể sử dụng Samsung Galaxy A30s khá thoải mái trong hơn một ngày là điều hết sức bình thường.
Ngoài ra, chiếc smartphone Samsung này còn được hỗ trợ sạc nhanh 15W, vì vậy nếu pin hết, nó sẽ dễ dàng sạc đầy pin lại.
Galaxy A30s cho khả năng sạc lại từ 0% đến 100% trong 1,5 giờ. Đây là một điểm cộng nữa trên chiếc máy này.

Màn hình lớn, vân tay thời thượng
Có thể bạn sẽ bất ngờ bởi chiếc Galaxy A30s mới sở hữu cho mình màn hình có kích thước lên tới 6.4 inch, còn lớn hơn cả chiếc Samsung Galaxy Note 10.
Với tỷ lệ màn hình 19:9 đảm bảo cho bạn có một không gian trải nghiệm rộng rãi trên kích thước 6 inch nhưng vẫn tối ưu được diện tích tổng thể thân máy.
Máy được trang bị vi xử lý Exynos 7904 có hiệu năng ổn định, vừa đủ để đáp ứng nhu cầu cơ bản hằng ngày như lướt web, xem phim hay chơi những game nhẹ như Hay Day, Candy Crush,...
Một điểm cộng khác của máy là tuy ở phân khúc tầm trung giá rẻ nhưng máy vẫn được Samsung ưu ái trang bị cảm biến vân tay bên trong màn hình.
Tất nhiên bạn vẫn sẽ có công nghệ nhận dạng khuôn mặt để mở khóa với tốc độ khá nhanh và chính xác.', 9, 17, 4000000, 4790000, CAST(N'2020-08-08T12:52:12.6754508' AS DateTime2), CAST(N'2020-08-08T12:52:12.6754429' AS DateTime2), 60, 12, 60)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (18, N'L2SHCDB102', N'Loa BLUETOOTH di động XB43 với EXTRA BASS', NULL, N'Nhật Bản', 1, 7, 11, 13, N'c0ebc50847b507a1e788c08c0530f599203345349.jpg', N'abcadas', 13, 9, 1200000, 32300000, CAST(N'2020-08-11T13:37:52.0000000' AS DateTime2), CAST(N'2020-08-22T20:33:45.3477827' AS DateTime2), 872, 12, 36)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (19, N'NTMEMTB103', N'Nhớt Motul 300V Factory Line 10W40 1L', NULL, N'Pháp', 6, 5, 12, 5, N'motul-300v-factory-line-10w40-1l-17-slide-products-5ddf48fbcbafe200609639.jpg', N'Nhớt Motul 300V Factory Line 10W40 1L được người dùng Việt Nam đánh giá là một trong những sản phẩm nhớt xe máy chất lượng cao cấp dành cho xe mô tô phân khối lớn tốt nhất hiện nay. 

Motul 300V 10W40 1L cũng là sản phẩm duy nhất trên thế giới đạt đến công nghệ ESTER Core, một công nghệ độc quyền của hãng dầu nhớt Motul, nâng tầm cải tiến và đáp ứng các yêu cầu kỹ thuật của những thế hệ động cơ đời mới nhất với tính năng bảo vệ chống mài mòn, chống giảm áp suất dầu và chống hiện tượng oxy hóa ở nhiệt độ cao, tối ưu hóa công suất và độ tin cậy cực cao.

Tính năng của dầu nhớt Motul 300V 10W40 FL Road Racing 1L
 
+ Motul 300V cho khả năng tối ưu hóa công suất động cơ và bảo vệ động cơ một cách hoàn hảo. 
+ Motul 300V giúp chịu được nhiệt độ cao.
+ Công nghệ trên Motul 300V kéo dài thời gian thay nhớt lên mức tối đa.

Thông số kĩ thuật của nhớt Motul 300V Factory Line 10W40 1L:
+ Nhà sản xuất: Tập đoàn Motul
+ Độ nhớt: 10W40
+ Dung tích nhớt: 1 lít
+ Dầu nhớt tổng hợp 100% Synthese
+ Tiêu chuẩn: API SN, JASO MA', 18, 23, 400000, 425000, CAST(N'2020-08-22T21:06:09.0000000' AS DateTime2), CAST(N'2020-08-22T21:07:22.1176418' AS DateTime2), 30, 12, 36)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (20, N'TCSMOMPP01', N'Mì khoai tây Omachi xốt bò hầm gói 80g', NULL, N'Việt Nam', 5, 1, 16, 8, N'mi-khoai-tay-omachi-xot-bo-ham-goi-80g-201912081333568103201229445.jpg', N'Mì khoai tây Omachi sinh ra từ lúa mì và tinh chất khoai tây, hòa quyện với trứng, ướp những hương liệu tuyệt vời, từng sợi mì Omachi vàng ươm dai ngon nay còn được đắm mình trong nước cốt từ thịt và xương nên càng đậm đà hấp dẫn.
Mì khoai tây Omachi Xốt bò Hầm với vị đậm đà của thịt bò hầm cùng vị ngọt bùi của cà rốt và vị the cay của ớt sẵn sàng làm xiêu lòng bất cứ ai.

Thành phần:
Vắt mì: Bột mì, dầu shortening, tinh chất từ bột khoai tây (10g/kg), muối tinh luyện, chất điều vị monosodium glutamate (621), bột trứng (1g/kg), chất tạo xốp pentasodium triphosphate (451i), chiết xuất trái dành dành, chất chống oxy hóa: BHA (320), BHT (321).
Súp: dầu cọ tinh luyện, tinh chất rau củ tươi, muối tinh luyện, đường, chất điều vị monosodium glutamate (621), nước cốt cô đặc từ thịt và xương (3.2g/kg), các loại rau sấy, thịt giả và các loại gia vị khác, bột thịt bò (3g/kg), chiết xuất nấm men, màu tự nhiên caramel (150a), chất điều vị: disodium guanilate (627), hương tổng hợp dùng trong thực phẩm, chất bảo quản sodium benzoate (211).', 10, 28, 5000, 10000, CAST(N'2020-08-22T21:12:29.4454510' AS DateTime2), CAST(N'2020-08-22T21:12:29.4454423' AS DateTime2), 10, 12, 24)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (21, N'DDMDDQWH02', N'Đèn thủy tinh trang trí Điện Quang ĐQ DCL02 WW', NULL, N'Việt Nam', 1, 7, 23, 7, N'62299054_ad95f1d8f4b04d9a97730f8a70a61dd4_master201945078.png', N'Đèn thủy tinh trang trí Điện Quang ĐQ DCL02 WW (thân hình bầu, NOEL 2, bóng LED, Warmwhite)
Đèn thủy tinh trang trí Điện Quang Noel khác biệt #hoàn_toàn với đèn trang trí thông thường.
Bạn sẽ cảm nhận được góc nho nhỏ cho phòng khách, phòng làm việc, phòng ngủ, cầu thang, cửa sổ …của căn hộ, nhà hàng, khách sạn của bạn thật ấm áp biết bao nhiêu. Đây cũng là món quà noel đầy ý nghĩa khi tặng cho người thân bạn bé.



1/ ĐIỂM ĐẶC BIỆT SẢN PHẨM:

Đèn trang trí được thiết kế nhỏ gọn sang trọng và tính thẩm mỹ cao
Dùng loại đui đèn xoắn E27 thông dụng trên thị trường
2/ THÔNG SỐ KỸ THUẬT:

* Thông số điện:

- Điện áp: 220V

- Tần số: 50Hz

- Công suất: 3W

* Thông số quang:

- Nhiệt độ màu: 2700K (warmwhite)

* Đóng gói:

- Kích thước hộp: 130 x 130 x 380mm', 16, 29, 443250, 450000, CAST(N'2020-08-22T21:19:45.0781737' AS DateTime2), CAST(N'2020-08-22T21:19:45.0781658' AS DateTime2), 300, 12, 60)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (22, N'TCSMSSGR01', N'Điện thoại Samsung Galaxy Note 20', NULL, N'Hàn Quốc', 2, 1, 9, 10, N'samsung-galaxy-note-20-062120-122128-400x460203057891.png', N'Camera cụm hình chữ nhật độc đáo cùng thiết kế mạnh mẽ
Điện thoại sở hữu thiết kế khung kim loại chắc chắn, mặt lưng nhựa bóng bẩy, kiểu dáng mạnh mẽ với những góc cạnh vuông vức nhưng vẫn mang lại cảm giác cầm nắm thoải mái.

Camera của Galaxy Note 20 được thiết kế trong cụm hình chữ nhật được đặt gọn ở phía sau bao gồm 1 camera chính 64 MP, camera góc siêu rộng 12 MP và camera tele 12 MP hỗ trợ người dùng dễ dàng lưu lại sắc nét những khoảng khắc đáng nhớ cùng gia đình và bạn bè.

Samsung Note 20 sở hữu camera trước 10 MP trong thiết kế nốt ruồi quen thuộc giúp khung màn hình 6.7 inch được sử dụng tối đa giúp cho việc xem phim hay chơi game đã mắt và tập trung hơn.

Công nghệ màn hình Super AMOLED Plus, độ phân giải Full HD+
Màn hình Samsung Galaxy Note 20 cho màu sắc chân thực, gần với thực tế mang đến những khung hình sống động và cực kì chi tiết, độ sáng màn hình cao hơn nên khi sử dụng dưới điều kiện nắng gắt cũng không ảnh hưởng nhiều đến chất lượng hiển thị.

Máy được tranh bị kính cảm ứng cường lực Gorilla Glass 6 cùng chuẩn chống nước và chống bụi là IP68, giúp cho người dùng có thể yên tâm khi dùng điện thoại ngoài đường hay vô tình tiếp xúc với nước.

Bảo mật vân tay trên màn hình nhanh chóng và tiện lợi
Tính năng bảo mật tiên tiến chắc chắn không thể thiếu trên Note 20, với công nghệ mở khóa bằng vân tay ngay trên màn hình giúp bạn mở khóa nhanh máy chỉ với một lần chạm vô cùng nhanh chóng và tiện lợi.

Dung lượng pin lớn, thoải mái sử dụng
Note 20 sở hữu cho mình viên pin dung lượng 4300 mAh, tăng thời lượng sử dụng lên tới 23% so với thế hệ Note 10 trước.

Bên cạnh đó sản phẩm còn hỗ trợ sạc pin nhanh, với khả năng sạc nhanh từ 0 đến 50% chỉ trong vòng 30 phút, giúp bạn rút ngắn được đáng kể thời gian chờ sạc cho thiết bị.

Có thể thấy, Samsung Note 20 là 1 trong những siêu phẩm đáng sở hữu nhất trong năm 2020 với thiết kế thu hút ở cụm camera độc đáo, màu sắc mới lạ, màn hình siêu tràn viền cùng với những tính năng tiện ích nâng cấp của bút S Pen.', 9, 17, 23990000, 23990000, CAST(N'2020-08-22T21:30:57.8917212' AS DateTime2), CAST(N'2020-08-22T21:30:57.8899740' AS DateTime2), 0, 12, 60)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (23, N'TCMETOWH01', N'Nồi cơm điện Toshiba 1 lít RC-10NMFVN(WT)', NULL, N'Thái Lan', 1, 5, 20, 7, N'10032712-noi-com-dien-toshiba-1l-rc-10nmfvn-wt-1203707684.jpg', N'Nồi cơm điện Toshiba 1 lít RC-10NMFVN(WT) sở hữu thiết kế nhỏ gọn, đơn giản với màu trắng sang trọng, mang đến vẻ đẹp tinh tế khi đặt trong căn bếp của bạn. Kết hợp cùng với bảng điều khiển điện tử, màn hình hiển thị LCD rõ ràng, cho bạn tiện theo dõi, dễ thao tác sử dụng.

Dung tích 1 lít của nồi cơm điện Toshiba đủ khả năng mang lại bữa cơm đủ đầy thơm nóng thích hợp cho gia đình từ 2 - 4 thành viên.

Chiếc nồi cơm điện được trang bị hệ thống hẹn giờ nấu thông minh với màn hình điện tử thể hiện thời gian nấu tiện lợi. Bên cạnh đó còn có nhiều chế độ nấu. Chỉ ấn nút và chờ cơm chín, quá dễ dàng và tiết kiệm thời gian.

Sản phẩm sở hữu 3 mâm nhiệt giúp gạo chín từ từ, cơm tơi xốp, không quá khô hay nhão. Trên nắp có khe thoát hơi kiểm soát tốt hơi nước trong nồi, chống tràn hiệu quả.

Nồi cơm điện Toshiba có lòng nồi dày gấp 3 lần các loại nồi thông thường, bền bỉ và dễ dàng vệ sinh làm sạch. Đáy nồi thiết kế tròn bầu, truyền nhiệt đều, nấu cơm chín ngon, tơi xốp hơn.', 16, 20, 2190000, 1720000, CAST(N'2020-08-22T21:37:07.6844581' AS DateTime2), CAST(N'2020-08-22T21:37:07.6844459' AS DateTime2), 35, 12, 120)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (24, N'GCMEASB103', N'Mainboard ASUS Z490 ROG MAXIMUS XII HERO (WI-FI)', NULL, N'Đài Loan', 1, 5, 21, 5, N'unnamed204250951.png', N'Bo mạch chủ ASUS ROG MAXIMUS XII HERO (WI-FI) với khả năng làm mát toàn diện và tính năng cấp điện cải tiến nhằm cung cấp năng lượng cho các vi xử lý đa nhân cùng tính năng hỗ trợ lưu trữ và bộ nhớ nhanh hơn, sẽ mang đến cho bạn tất cả mọi thứ cần thiết để phát huy tối đa sức mạnh của các linh kiện trong giàn máy của bạn nhằm đạt được hiệu năng chơi game tốt nhất.

ASUS ROG MAXIMUS XII HERO (WI-FI) được trang bị các tính năng tốt nhất để tăng cường trải nghiệm chơi game của game thủ như khả năng tản nhiệt, khả năng lưu trữ, khả năng kết nối, chất lượng âm thanh. Việc bạn đăng nhập vào game nhanh hơn, độ trễ thấp hơn, âm thanh rõ nét hơn sẽ giúp bạn dành chiến thắng dễ dàng hơn.

Với thiết kế bao gồm hàng loạt đường nét hoa văn hiện đại đi kèm với khả năng tùy biến cao, ASUS ROG MAXIMUS XII HERO (WI-FI) giúp bạn lắp ráp và cá nhân hóa giàn máy chơi game cực kỳ dễ dàng nhờ danh sách kiểm chuẩn toàn diện và hệ sinh thái các linh kiện đa dạng nhất trong ngành.

Bo mạch chủ ASUS ROG MAXIMUS XII HERO (WI-FI) trang bị các trình điều khiển firmware và tiện ích phần mềm được thiết kế dành cho tất cả các mức độ kỹ năng, thực hiện cài đặt, hiệu chỉnh và bảo trì hệ thống đơn giản. Với các tùy chọn từ ép xung, tản nhiệt cho đến quản lý mạng và các đặc điểm âm thanh, bạn có thể cấu hình giàn máy chơi game theo cách mà bạn muốn.', 17, 30, 10580000, 9990000, CAST(N'2020-08-22T21:42:50.9512258' AS DateTime2), CAST(N'2020-08-22T21:42:50.9512131' AS DateTime2), 1262, 36, 120)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (25, N'VGMEASB102', N'Card màn hình ASUS GeForce RTX 2080Ti 11GB GDDR6 ROG Strix OC (ROG-STRIX-RTX2080TI-O11G-GAMING)', NULL, N'Đài Loan', 2, 5, 21, 5, N'sdfdsfsreassd204840144.jpg', N'GeForce RTX 2080Ti 11GB GDDR6 ROG Strix OC có thể được xem là chiếc card đồ họa nhanh nhất và tốt nhất được ASUS sản xuất tính đến thời điểm hiện tại, sử dụng bộ xử lý đồ họa RTX 2080Ti với tốc độ xử lý tuyệt vời và hỗ trợ nhiều công nghệ mới nhất của NVIDIA như DLSS, Ray tracing, VRS đem lại trải nghiệm chơi game tốt nhất tới tay game thủ.

ASUS GeForce RTX 2080Ti 11GB GDDR6 ROG Strix OC mang trong mình thiết kế góc cạnh khỏe khoắn đặc thù của dòng sản phẩm ROG, cộng thêm hệ thống RGB LED và 3 quạt tản nhiệt hầm hố giúp tạo nên điểm nổi bật so với các đối thủ cạnh tranh.

Tuy nhiên, điểm nổi bật của ASUS GeForce RTX 2080Ti 11GB GDDR6 ROG Strix OC không chỉ dừng lại ở phần thiết kế bên ngoài, mà bên trong là cả 1 hệ thống tản nhiệt được tiết kế tỉ mỉ đến từng chi tiết với 6 ống dẫn nhiệt bằng đồng nguyên chất tiếp xúc trực tiếp với bộ xử lý đồ họa, dẫn nhiệt tới hàng loạt các lá nhôm tản nhiệt có diện tích tiếp xúc lớn kết hợp với 3 quạt làm mát có lực ép lớn đem lại hiệu năng tản nhiệt tuyệt vời.

Không những vậy, ASUS GeForce RTX 2080Ti 11GB GDDR6 ROG Strix OC còn được sử dụng thêm 1 tấm "back plate" bằng kim loại phía sau bo mạch và 1 tấm kim loại phía trước nằm trên bo mạch vừa đóng vai trò tản nhiệt cho chip nhớ và dàn cấp nguồn vừa kết hợp với tấm "back plate" ở phía sau để tăng cường độ cứng cáp của bo mạch.

Đi với GeForce RTX 2080Ti 11GB GDDR6 ROG Strix OC là 3 chiếc quạt làm mát được ASUS thiết kế với hình dáng được tối ưu cho việc tạo lực ép của không khí xuống tản nhiệt. Giúp áp lưc không khí được cải thiện 105% so với các thiết kế quạt truyền thống, thậm chí phần trục của cả 3 chiếc quạt làm mát này còn đạt chuẩn kháng bụi IP5X, nhằm đảm bảo tuổi thọ và khả năng hoạt động ổn định.
', 17, 31, 43000000, 43300000, CAST(N'2020-08-22T21:48:40.1445901' AS DateTime2), CAST(N'2020-08-22T21:48:40.1445802' AS DateTime2), 0, 36, 120)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (26, N'VGSMS0G202', N'Card màn hình ASUS GeForce GTX 1650 4GB GDDR5 DUAL (DUAL-GTX1650-4G)', NULL, N'Đài Loan', 2, 5, 21, 2, N'dsp205518800.jpg', N'GTX 1650 4GB GDDR5 DUAL là chiếc card màn hình tầm trung mới nhất của ASUS, trang bị bộ xử lý GTX 1650 với hiệu năng được cải thiện rất nhiều so với thế hệ trước là GTX 1050, thậm chí còn đem lại hiệu năng chơi game trên độ phân giải 1080p cao hơn đáng kể so với GTX 1050Ti, kết hợp thiết kế 2 quạt nhỏ gọn giúp tối ưu khả năng tản nhiệt đồng thời vẫn đảm bảo tương thích tốt với nhiều mẫu case có kích thước nhỏ.

Với việc đạt chuẩn kháng bụi IP5X, hạn chế việc bụi lọt vào trục của quạt đảm bảo đem lại tuổi thọ hoạt động lâu dài cho quạt làm mát.

Toàn bộ các dòng card màn hình của ASUS đều được trải qua dây chuyển sản xuất tự động, han chế tối đa các sai sót có thể xảy ra đồng thời mỗi chiếc card màn hình trước khi tới tay người dùng đều phải trải qua quy trình kiểm định nghiêm ngặt.', 17, 30, 4000000, 4190000, CAST(N'2020-08-22T21:55:18.8009058' AS DateTime2), CAST(N'2020-08-22T21:55:18.8008980' AS DateTime2), 123, 36, 120)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (27, N'DDSMPHWH02', N'Bộ Đèn Bàn UV-C Philips Khử Trùng Căn Phòng Chỉ Trong Vài Phút', NULL, N'Việt Nam', 1, 1, 22, 7, N'den-ban-khu-trung-UVC-2-600x619205854764.jpg', N'Loại đèn: Bộ đèn bàn UV-C khử trùng
Điện nguồn: 220-240Vac 50/60Hz
Công suất: 24W
Tuổi thọ: 9000 giờ
Kích thước: 120 x120 x 247mm
Trọng lượng: 1200g
Màu: sắc Bạc
Bảo hành: 24 tháng', 16, 29, 3100000, 2200000, CAST(N'2020-08-22T21:58:54.7641072' AS DateTime2), CAST(N'2020-08-22T21:58:54.7640965' AS DateTime2), 2324, 24, 60)
INSERT [dbo].[materials] ([Id], [MatCode], [MatName], [Note], [Locations], [Units], [SizesMat], [BrandsMat], [ColorsMat], [Images], [Descriptions], [GruopMat], [TypeMat], [PrimePrice], [SellPrice], [CreatedAt], [UpdateAt], [Quantity], [Warranty], [Duration]) VALUES (28, N'L2XSL2G202', N'Galaxy A51', NULL, N'Việt Nam', 2, 1, 2, 2, N'logoTeam10202452103.png', N'abc', 3, 11, 65000, 7032500, CAST(N'2020-08-27T13:24:52.0000000' AS DateTime2), CAST(N'2020-08-27T13:25:33.8016585' AS DateTime2), 10, 12, 36)
SET IDENTITY_INSERT [dbo].[materials] OFF
GO
SET IDENTITY_INSERT [dbo].[mats_proces] ON 

INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (46, 8, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (57, 5, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (58, 5, 2, 1)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (59, 6, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (60, 6, 2, 1)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (61, 6, 2, 2)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (67, 10, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (68, 10, 2, 1)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (101, 11, 3, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (102, 12, 3, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (103, 13, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (104, 14, 3, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (105, 15, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (106, 16, 3, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (107, 17, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (108, 18, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (109, 19, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (110, 20, 2, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (111, 21, 8, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (112, 22, 5, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (113, 23, 2, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (114, 24, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (115, 25, 2, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (116, 26, 2, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (117, 26, 1, 1)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (118, 27, 8, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (119, 27, 1, 1)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (120, 28, 1, 0)
INSERT [dbo].[mats_proces] ([Id], [MatId], [ProceId], [LevelProce]) VALUES (121, 28, 2, 1)
SET IDENTITY_INSERT [dbo].[mats_proces] OFF
GO
SET IDENTITY_INSERT [dbo].[procedures] ON 

INSERT [dbo].[procedures] ([Id], [ProcedureCode], [ProcedureName], [Quantity], [ParentCode]) VALUES (1, N'L02', N'Lô', 10, NULL)
INSERT [dbo].[procedures] ([Id], [ProcedureCode], [ProcedureName], [Quantity], [ParentCode]) VALUES (2, N'L023', N'Thùng', 20, NULL)
INSERT [dbo].[procedures] ([Id], [ProcedureCode], [ProcedureName], [Quantity], [ParentCode]) VALUES (3, N'C1', N'Cái', 1, NULL)
INSERT [dbo].[procedures] ([Id], [ProcedureCode], [ProcedureName], [Quantity], [ParentCode]) VALUES (4, N'CH100', N'Chiếc', 100, NULL)
INSERT [dbo].[procedures] ([Id], [ProcedureCode], [ProcedureName], [Quantity], [ParentCode]) VALUES (5, N'CH1', N'Chiếc', 1, NULL)
INSERT [dbo].[procedures] ([Id], [ProcedureCode], [ProcedureName], [Quantity], [ParentCode]) VALUES (6, N'CH10', N'Chiếc', 10, NULL)
INSERT [dbo].[procedures] ([Id], [ProcedureCode], [ProcedureName], [Quantity], [ParentCode]) VALUES (7, N'C10', N'Cái', 10, NULL)
INSERT [dbo].[procedures] ([Id], [ProcedureCode], [ProcedureName], [Quantity], [ParentCode]) VALUES (8, N'C100', N'Cái', 100, NULL)
SET IDENTITY_INSERT [dbo].[procedures] OFF
GO
SET IDENTITY_INSERT [dbo].[ReceiptDetails] ON 

INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (16, 12, 6, 20, CAST(N'2020-07-10T15:48:14.7991933' AS DateTime2), 61, 1, 25000, 500000, 20)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (17, 12, 5, 10, CAST(N'2020-07-10T15:48:14.9220829' AS DateTime2), 58, 1, 57500, 1150000, 20)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (18, 13, 6, 20, CAST(N'2020-07-10T15:48:41.2728268' AS DateTime2), 59, 1, 25000, 100000000, 4000)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (26, 15, 6, 20, CAST(N'2020-07-17T11:40:45.0000000' AS DateTime2), 60, 1, 25000, 10000000, 400)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (54, 14, 6, 20, CAST(N'2020-07-10T15:50:31.0000000' AS DateTime2), 59, 2, 25000, 200000000, 8000)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (55, 17, 6, 20, CAST(N'2020-07-17T12:25:19.0000000' AS DateTime2), 60, 2, 25000, 10000000, 400)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (58, 16, 6, 20, CAST(N'2020-07-17T12:05:58.0000000' AS DateTime2), 61, 2, 25000, 1000000, 40)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (59, 16, 5, 10, CAST(N'2020-07-17T12:05:58.0000000' AS DateTime2), 58, 1, 53750, 21500000, 400)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (67, 22, 15, 36, CAST(N'2020-08-11T17:50:02.0000000' AS DateTime2), 105, 2, 270000, 5400000, 20)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (69, 20, 11, 24, CAST(N'2020-08-11T16:55:08.0000000' AS DateTime2), 101, 2, 1300000, 26000000, 20)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (74, 21, 16, 60, CAST(N'2020-08-11T17:26:21.0000000' AS DateTime2), 106, 1, 42900000, 429000000, 10)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (75, 23, 6, 20, CAST(N'2020-08-22T20:22:28.9680502' AS DateTime2), 59, 2, 25000, 200000000, 8000)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (76, 23, 18, 36, CAST(N'2020-08-22T20:22:28.9719806' AS DateTime2), 108, 13, 32300000, 4199000000, 130)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (77, 11, 6, 20, CAST(N'2020-07-10T15:41:25.0000000' AS DateTime2), 61, 1, 25000, 500000, 20)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (78, 11, 5, 10, CAST(N'2020-07-10T15:41:25.0000000' AS DateTime2), 58, 1, 57500, 1150000, 20)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (79, 18, 5, 10, CAST(N'2020-07-19T12:29:47.0000000' AS DateTime2), 57, 601, 53750, 6460750000, 120200)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (80, 24, 18, 36, CAST(N'2020-08-22T22:07:32.9949633' AS DateTime2), 108, 2, 32300000, 646000000, 20)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (81, 24, 25, 120, CAST(N'2020-08-22T22:07:33.0142672' AS DateTime2), 115, 5, 43300000, 4330000000, 100)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (82, 25, 19, 36, CAST(N'2020-08-22T22:22:45.3405003' AS DateTime2), 109, 1, 425000, 4250000, 10)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (83, 25, 22, 60, CAST(N'2020-08-22T22:22:45.3412653' AS DateTime2), 112, 1, 23990000, 23990000, 1)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (84, 26, 17, 60, CAST(N'2020-08-22T22:28:46.0650513' AS DateTime2), 107, 1, 4790000, 47900000, 10)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (85, 26, 24, 120, CAST(N'2020-08-22T22:28:46.0658589' AS DateTime2), 114, 1, 9990000, 99900000, 10)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (86, 27, 11, 24, CAST(N'2020-08-22T22:35:01.8137727' AS DateTime2), 101, 2, 1300000, 2600000, 2)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (87, 27, 21, 60, CAST(N'2020-08-22T22:35:01.8145377' AS DateTime2), 111, 2, 450000, 90000000, 200)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (88, 28, 23, 120, CAST(N'2020-08-22T22:42:19.4458299' AS DateTime2), 113, 1, 1720000, 34400000, 20)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (89, 28, 24, 120, CAST(N'2020-08-22T22:42:19.4465101' AS DateTime2), 114, 2, 9990000, 199800000, 20)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (90, 29, 12, 24, CAST(N'2020-08-22T22:46:56.7618088' AS DateTime2), 102, 2, 30000, 60000, 2)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (91, 29, 14, 24, CAST(N'2020-08-22T22:46:56.7623841' AS DateTime2), 104, 2, 150000, 300000, 2)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (95, 30, 5, 10, CAST(N'2020-08-25T14:42:40.0000000' AS DateTime2), 61, 3, 53750, 3225000, 60)
INSERT [dbo].[ReceiptDetails] ([Id], [Receipt_Id], [Materials_Id], [Duration], [CreatedAt], [Procedure_Materials_Id], [Quantity], [Price], [TotalPrice], [Amount]) VALUES (97, 31, 28, 36, CAST(N'2020-08-27T13:27:53.0000000' AS DateTime2), 121, 7, 7032500, 984550000, 140)
SET IDENTITY_INSERT [dbo].[ReceiptDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[Receipts] ON 

INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (11, N'123', N'Huy Le', N'NM10072020', N'123', N'12', N'1234', N'123', N'lightof99@gmail.com', N'0785555772', N'123', CAST(N'2020-07-10T00:00:00.0000000' AS DateTime2), CAST(N'2020-07-10T15:41:25.0000000' AS DateTime2), CAST(N'2020-08-22T20:31:59.6587918' AS DateTime2), 1650000, NULL, NULL, 3, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (12, N'123', N'Huy Le', N'XK10072020034814', N'123', N'123', N'1234', N'123', N'lightof99@gmail.com', N'0785555772', N'123', CAST(N'2020-07-10T00:00:00.0000000' AS DateTime2), CAST(N'2020-07-10T15:48:14.9237227' AS DateTime2), CAST(N'2020-07-10T15:48:14.9235855' AS DateTime2), 1650000, NULL, NULL, 3, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (13, N'123', N'Huy Le', N'NM10072020034841', N'123', N'123', N'1234', N'123', N'lightof99@gmail.com', N'0785555772', N'123', CAST(N'0001-01-26T00:00:00.0000000' AS DateTime2), CAST(N'2020-07-10T15:48:41.2737360' AS DateTime2), CAST(N'2020-07-10T15:48:41.2737255' AS DateTime2), 100000000, NULL, NULL, 1, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (14, N'123', N'Huy Le5', N'NT10072020035031', N'123', N'123', N'1234', N'we', N'lightof99@gmail.com', N'0785555772', N'123', CAST(N'2020-07-10T00:00:00.0000000' AS DateTime2), CAST(N'2020-07-10T15:50:31.0000000' AS DateTime2), CAST(N'2020-07-19T13:08:16.1508078' AS DateTime2), 200000000, NULL, NULL, 2, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (15, N'123', N'Huy Le', N'XK17072020114045', N'123', N'123', N'1234', N'123', N'lightof99@gmail.com', N'0785555772', N'123', CAST(N'2020-07-17T00:00:00.0000000' AS DateTime2), CAST(N'2020-07-17T11:40:45.0000000' AS DateTime2), CAST(N'2020-07-17T12:05:00.6205678' AS DateTime2), 10000000, NULL, NULL, 3, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (16, N'123', N'Huy Le', N'XK17072020120558', N'123', N'12', N'1234', N'123', N'lightof99@gmail.com', N'0785555772', N'123', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2020-07-17T12:05:58.0000000' AS DateTime2), CAST(N'2020-08-07T21:06:23.6662754' AS DateTime2), 22500000, NULL, NULL, 3, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (17, N'123', N'Huy Le5', N'NM17072020122519', N'123', N'123', N'12343', N'1233', N'lightof99@gmail.com', N'0785555772', N'123', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2020-07-17T12:25:19.0000000' AS DateTime2), CAST(N'2020-07-19T16:47:04.5483813' AS DateTime2), 10000000, NULL, NULL, 1, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (18, N'21312', N'ewqe', N'NM19072020122947', N'3213', N'wqe', N'213', N'231312', N'wqeda@gmail.com', N'321', N'sad', CAST(N'2020-07-19T14:02:00.0000000' AS DateTime2), CAST(N'2020-07-19T12:29:47.0000000' AS DateTime2), CAST(N'2020-08-22T20:34:07.6769305' AS DateTime2), 6460750000, N'dde', NULL, 1, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (20, N'KH01', N'Le Khac Huy', N'NM11082020045508', N'Microsystem', N'CT01', N'CTK01', N'Nguyen Van An', N'annguyen123@gmial.com', N'09402394', N'342 Phan Huy Ích, phường 12, quận Gò Vấp', CAST(N'2020-08-11T12:00:00.0000000' AS DateTime2), CAST(N'2020-08-11T16:55:08.0000000' AS DateTime2), CAST(N'2020-08-14T13:01:04.1529189' AS DateTime2), 26000000, N'abc', NULL, 1, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (21, N'KH02', N'Dang Cuong Quoc', N'XK11082020052621', N'PCB Corporation', N'CT02', N'CTK02', N'Le Khac Huy', N'lightof123@gmail.com', N'0854325350', N'32 Lê Văn Sĩ, Phường 1, Quận Tân Bình', CAST(N'2020-08-11T14:00:00.0000000' AS DateTime2), CAST(N'2020-08-11T17:26:21.0000000' AS DateTime2), CAST(N'2020-08-22T20:19:20.2048129' AS DateTime2), 429000000, N'abc', NULL, 3, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (22, N'KHT01', N'Nguyen Van An', N'NT11082020055002', N'TPS Corporation', N'CTT01', N'CTTK01', N'Le Vu An', N'anvu123@gmail.com', N'094345443', N'543 Trường Chinh, Phường 12, quận Tân Bình, TPHCM', CAST(N'2020-08-11T13:02:00.0000000' AS DateTime2), CAST(N'2020-08-11T17:50:02.0000000' AS DateTime2), CAST(N'2020-08-11T17:54:31.8960042' AS DateTime2), 5400000, N'abcdef', NULL, 2, 0)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (23, N'C23049', N'Cường Quốc', N'XK22082020082228', N'VTH Inc', N'CT2039', N'CTK23003', N'Cường quốc', N'qcuoc@gmail.com', N'0129382930', N'53/4 phạm ngũ lại phường Cô bắc Quận 1', CAST(N'2020-08-20T00:00:00.0000000' AS DateTime2), CAST(N'2020-08-22T20:22:28.9724420' AS DateTime2), CAST(N'2020-08-22T20:22:28.9724408' AS DateTime2), 4399000000, NULL, N'Nhập mới', 3, 1)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (24, N'KHX0034', N'Đặng Cường Quốc', N'XK22082020100732', N'phongvu.vn', N'CT00054', N'CTKX0054', N'Mai Hoàng An', N'lightof23@gmail.com', N'079543080', N'432 Nguyễn Văn Lượng, Phường 12, quận Gò Vấp', CAST(N'2020-08-22T00:00:00.0000000' AS DateTime2), CAST(N'2020-08-22T22:07:33.0146240' AS DateTime2), CAST(N'2020-08-22T22:07:33.0146233' AS DateTime2), 4976000000, NULL, NULL, 3, 1)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (25, N'KHX0046', N'Lê Khắc Huy', N'XK22082020102245', N'ABC Corporation', N'CTX0046', N'CTKX0046', N'Lê Vũ An', N'levuan123@gmail.com', N'096523424', N'67 Nguyễn Thái Sơn, Phường 9, quận Gò Vấp, TPHCM', CAST(N'2020-08-16T13:02:00.0000000' AS DateTime2), CAST(N'2020-08-22T22:22:45.3414939' AS DateTime2), CAST(N'2020-08-22T22:22:45.3414932' AS DateTime2), 28240000, NULL, NULL, 3, 1)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (26, N'CTM0048', N'Tô Long Hồ', N'NM22082020102846', N'Microtek Corporation', N'CTM0048', N'CTKM0048', N'Trần Quốc Nhật Hào', N'haotran2912@gmail.com', N'095412343', N'226 Lê Văn Sỹ, phường 1, quận Tân Bình, TPHCM', CAST(N'2020-08-13T14:03:00.0000000' AS DateTime2), CAST(N'2020-08-22T22:28:46.0664144' AS DateTime2), CAST(N'2020-08-22T22:28:46.0664089' AS DateTime2), 147800000, NULL, NULL, 1, 1)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (27, N'KHT0049', N'Lê Vũ An', N'NT22082020103501', N'Net Power', N'CTT0049', N'CTTK0049', N'Đặng Cường Quốc', N'cuongquoc123@gmail.com', N'034567623', N'456 Nguyễn Thái Bình, phường 12, quận Tân Bình, TPHCM', CAST(N'2020-08-02T00:00:00.0000000' AS DateTime2), CAST(N'2020-08-22T22:35:01.8149501' AS DateTime2), CAST(N'2020-08-22T22:35:01.8149450' AS DateTime2), 92600000, NULL, NULL, 2, 1)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (28, N'KHT0021', N'Tô Long Hồ', N'NT22082020104219', N'KN Corporation', N'CT0049', N'CTKT0049', N'Lê Khắc Huy', N'huyle156@gmail.com', N'054389543', N'436 Phạm Thế Hiển, phường 5, quận 8, TPHCM', CAST(N'2020-08-03T19:00:00.0000000' AS DateTime2), CAST(N'2020-08-22T22:42:19.4467151' AS DateTime2), CAST(N'2020-08-22T22:42:19.4467148' AS DateTime2), 234200000, NULL, NULL, 2, 1)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (29, N'KHT0098', N'Nguyễn Phương Lan', N'NT22082020104656', N'ACV Group', N'CTT0098', N'CTKT0098', N'Tô Long Hồ', N'longho76@gmail.com', N'0549564657', N'43 Dương Quảng Hàm, phường 5, quận Gò Vấp, TPHCM', CAST(N'2020-08-12T14:01:00.0000000' AS DateTime2), CAST(N'2020-08-22T22:46:56.7626991' AS DateTime2), CAST(N'2020-08-22T22:46:56.7626987' AS DateTime2), 360000, NULL, NULL, 2, 1)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (30, N'kajsdhfkj', N'm,jnbasdjkfb,v', N'NM25082020024240', N'jasdfkjhasdfk', N'44', N'44', N'álkdjflihasdf', N'dfkhasdlkf@gmail.com', N'0902733086', N'44444', CAST(N'2020-08-24T00:00:00.0000000' AS DateTime2), CAST(N'2020-08-25T14:42:40.0000000' AS DateTime2), CAST(N'2020-08-25T15:54:45.4223654' AS DateTime2), 3225000, NULL, NULL, 1, 1)
INSERT [dbo].[Receipts] ([Id], [CustomerCode], [CustomerName], [ReceiptCode], [CompanyName], [LicensesIn], [LicensesCheck], [Receiver], [Email], [Phone], [Adress], [ReceiveDay], [CreatedAt], [UpdateAt], [TotalPrice], [Description], [Note], [Type], [IsEdit]) VALUES (31, N'KH00143', N'Nguyen Van A', N'XK27082020012753', N'ABC Corporation', N'CT00143', N'CTK00143', N'Le Van B', N'vanb@gmail.com', N'08943242', N'324 Nguyen Van Luong', CAST(N'2020-08-27T00:00:00.0000000' AS DateTime2), CAST(N'2020-08-27T13:27:53.0000000' AS DateTime2), CAST(N'2020-08-27T13:30:25.3509327' AS DateTime2), 984550000, NULL, NULL, 3, 1)
SET IDENTITY_INSERT [dbo].[Receipts] OFF
GO
SET IDENTITY_INSERT [dbo].[sizes] ON 

INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (1, N'XS', N'AA')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (2, N'XL', N'Lớn')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (3, N'LO', N'Dài')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (4, N'SH', N'Ngắn')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (5, N'ME', N'Vừa')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (7, N'MD', N'Trung Bình')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (8, N'LL', N'LARGE')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (9, N'MM', N'Medium')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (10, N'AA', N'Medium')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (11, N'TT', N'TB')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (12, N'BB', N'BB')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (13, N'AC', N'C')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (14, N'DD', N'DD')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (15, N'ee', N'EE')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (16, N'FF', N'FF')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (17, N'GG', N'GG')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (18, N'HH', N'HH')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (19, N'LA', N'LL')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (20, N'KK', N'KK')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (21, N'II', N'II')
INSERT [dbo].[sizes] ([Id], [SizeCode], [SizeName]) VALUES (22, N'BT', N'TT')
SET IDENTITY_INSERT [dbo].[sizes] OFF
GO
SET IDENTITY_INSERT [dbo].[unitsmetrics] ON 

INSERT [dbo].[unitsmetrics] ([Id], [UnitsCode], [UnitsName]) VALUES (1, N'G0', N'Cái')
INSERT [dbo].[unitsmetrics] ([Id], [UnitsCode], [UnitsName]) VALUES (2, N'G2', N'Chiếc')
INSERT [dbo].[unitsmetrics] ([Id], [UnitsCode], [UnitsName]) VALUES (3, N'T0', N'Thùng')
INSERT [dbo].[unitsmetrics] ([Id], [UnitsCode], [UnitsName]) VALUES (4, N'LO', N'Lô ')
INSERT [dbo].[unitsmetrics] ([Id], [UnitsCode], [UnitsName]) VALUES (5, N'GI', N'Gói')
INSERT [dbo].[unitsmetrics] ([Id], [UnitsCode], [UnitsName]) VALUES (6, N'CI', N'Chai')
INSERT [dbo].[unitsmetrics] ([Id], [UnitsCode], [UnitsName]) VALUES (7, N'LM', N'Lọ')
SET IDENTITY_INSERT [dbo].[unitsmetrics] OFF
GO
INSERT [dbo].[Users] ([Id], [Usersname], [Password], [Name], [Address], [Email], [Phone], [isDeactive], [Image], [Role]) VALUES (N'099fbbe5-749f-4945-bf83-89992e891fbe', N'cuongquoc99', N'123456', N'Đặng Cường Quốc', N'321 abc', N'quoccuong123@gmail.com', N'0342495483', 0, N'images204439490.jpg', N'User')
INSERT [dbo].[Users] ([Id], [Usersname], [Password], [Name], [Address], [Email], [Phone], [isDeactive], [Image], [Role]) VALUES (N'2c98d370-8e94-40de-9a28-8e4609863809', N'admin', N'123456', N'admin', N'fake', N'lgoagn@gmail.com', N'123456789', 0, N'logo-inverse200302737.png', N'ADMIN')
INSERT [dbo].[Users] ([Id], [Usersname], [Password], [Name], [Address], [Email], [Phone], [isDeactive], [Image], [Role]) VALUES (N'3b0d2aa1-4696-49ea-b0a5-395738499982', N'nguyenvanan', N'123456', N'Nguyễn Văn An', N'54 Nguyễn Văn Lượng, Phường 15, Quận Gò Vấp, Thành phố Hồ Chí Minh', N'nguyenan123@gmail.com', N'0797344325', 1, N'Chroma-crystal_3840x2160203231080.png', N'User')
INSERT [dbo].[Users] ([Id], [Usersname], [Password], [Name], [Address], [Email], [Phone], [isDeactive], [Image], [Role]) VALUES (N'3edeb976-55be-43b3-b910-cb36a508be37', N'lekhachuy555', N'13123', N'Huy Le', N'2131 ABC', N'lightof99@gmail.com', N'0785555772', 1, N'logo-inverse204440264.png', N'User')
INSERT [dbo].[Users] ([Id], [Usersname], [Password], [Name], [Address], [Email], [Phone], [isDeactive], [Image], [Role]) VALUES (N'57ce9535-c92e-42d6-87ac-7982df4a5332', N'newaccount', N'123456', N'mpgmasfm', N'213', N'lightof99@gmail.com', N'0785555772', 0, N'logo200101694.png', N'User')
INSERT [dbo].[Users] ([Id], [Usersname], [Password], [Name], [Address], [Email], [Phone], [isDeactive], [Image], [Role]) VALUES (N'afc243ee-e802-4e28-9626-2fc7e5790b10', N'tranquocnhathao', N'123456', N'Trần Quốc Nhật Hào', N'321 Nguyễn Văn Lượng', N'tranquocnhathao@gmail.com', N'098634564', 0, N'LG G6 Stock Wallpapers (2)200229440.png', N'User')
INSERT [dbo].[Users] ([Id], [Usersname], [Password], [Name], [Address], [Email], [Phone], [isDeactive], [Image], [Role]) VALUES (N'ea291946-b544-4f55-be73-9b18702a6a75', N'maihoangan', N'123456789', N'Mai Hoàng An', N'327/39 sư vạn hạnh', N'maihoangan1999@gmail.com', N'0902770317', 0, N'IMG_20180918_213708_483201132178.jpg', N'User')
INSERT [dbo].[Users] ([Id], [Usersname], [Password], [Name], [Address], [Email], [Phone], [isDeactive], [Image], [Role]) VALUES (N'f5fbfa53-0649-4340-986e-a1a7a1afd648', N'lekhachuy5', N'123456', N'Huy Le', N'2132', N'lightof99@gmail.com', N'0785555772', 0, N'755326201309532.jpg', N'User')
GO
ALTER TABLE [dbo].[Users] ADD  DEFAULT (CONVERT([bit],(0))) FOR [isDeactive]
GO
ALTER TABLE [dbo].[categorytypes]  WITH CHECK ADD  CONSTRAINT [FK_CateType_CateGroup] FOREIGN KEY([Group_Id])
REFERENCES [dbo].[categorygroups] ([Id])
GO
ALTER TABLE [dbo].[categorytypes] CHECK CONSTRAINT [FK_CateType_CateGroup]
GO
ALTER TABLE [dbo].[materials]  WITH CHECK ADD  CONSTRAINT [FK_Mat_Brands] FOREIGN KEY([BrandsMat])
REFERENCES [dbo].[brands] ([Id])
GO
ALTER TABLE [dbo].[materials] CHECK CONSTRAINT [FK_Mat_Brands]
GO
ALTER TABLE [dbo].[materials]  WITH CHECK ADD  CONSTRAINT [FK_Mat_Colors] FOREIGN KEY([ColorsMat])
REFERENCES [dbo].[colors] ([Id])
GO
ALTER TABLE [dbo].[materials] CHECK CONSTRAINT [FK_Mat_Colors]
GO
ALTER TABLE [dbo].[materials]  WITH CHECK ADD  CONSTRAINT [FK_Mat_Groups] FOREIGN KEY([GruopMat])
REFERENCES [dbo].[categorygroups] ([Id])
GO
ALTER TABLE [dbo].[materials] CHECK CONSTRAINT [FK_Mat_Groups]
GO
ALTER TABLE [dbo].[materials]  WITH CHECK ADD  CONSTRAINT [FK_Mat_Sizes] FOREIGN KEY([SizesMat])
REFERENCES [dbo].[sizes] ([Id])
GO
ALTER TABLE [dbo].[materials] CHECK CONSTRAINT [FK_Mat_Sizes]
GO
ALTER TABLE [dbo].[materials]  WITH CHECK ADD  CONSTRAINT [FK_Mat_Types] FOREIGN KEY([TypeMat])
REFERENCES [dbo].[categorytypes] ([Id])
GO
ALTER TABLE [dbo].[materials] CHECK CONSTRAINT [FK_Mat_Types]
GO
ALTER TABLE [dbo].[materials]  WITH CHECK ADD  CONSTRAINT [FK_Mat_Units] FOREIGN KEY([Units])
REFERENCES [dbo].[unitsmetrics] ([Id])
GO
ALTER TABLE [dbo].[materials] CHECK CONSTRAINT [FK_Mat_Units]
GO
ALTER TABLE [dbo].[mats_proces]  WITH CHECK ADD  CONSTRAINT [FK_matproc_Mat] FOREIGN KEY([MatId])
REFERENCES [dbo].[materials] ([Id])
GO
ALTER TABLE [dbo].[mats_proces] CHECK CONSTRAINT [FK_matproc_Mat]
GO
ALTER TABLE [dbo].[mats_proces]  WITH CHECK ADD  CONSTRAINT [FK_matproc_Pro] FOREIGN KEY([ProceId])
REFERENCES [dbo].[procedures] ([Id])
GO
ALTER TABLE [dbo].[mats_proces] CHECK CONSTRAINT [FK_matproc_Pro]
GO
ALTER TABLE [dbo].[ReceiptDetails]  WITH CHECK ADD  CONSTRAINT [FK_Material_Receipt] FOREIGN KEY([Materials_Id])
REFERENCES [dbo].[materials] ([Id])
GO
ALTER TABLE [dbo].[ReceiptDetails] CHECK CONSTRAINT [FK_Material_Receipt]
GO
ALTER TABLE [dbo].[ReceiptDetails]  WITH CHECK ADD  CONSTRAINT [FK_Pro_Details] FOREIGN KEY([Procedure_Materials_Id])
REFERENCES [dbo].[mats_proces] ([Id])
GO
ALTER TABLE [dbo].[ReceiptDetails] CHECK CONSTRAINT [FK_Pro_Details]
GO
ALTER TABLE [dbo].[ReceiptDetails]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_Details] FOREIGN KEY([Receipt_Id])
REFERENCES [dbo].[Receipts] ([Id])
GO
ALTER TABLE [dbo].[ReceiptDetails] CHECK CONSTRAINT [FK_Receipt_Details]
GO
USE [master]
GO
ALTER DATABASE [SEP23Team10] SET  READ_WRITE 
GO
