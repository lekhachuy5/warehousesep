﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using netCoreTutorial.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using WareHouse.Controllers;
using WareHouse.Models;
using Xunit;

namespace warehouse.test
{
    public class CategoryGroupsControllerTests
    {



        [Fact]
        public async void IndexTest()
        {
            List<CategoryGroups> getList = new List<CategoryGroups>();
            IEnumerable<CategoryGroups> lists = getList;
            var mockres = new Mock<IGenericRepository<CategoryGroups>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.CategoryGroupsRepository).Returns(mockres.Object);

            CategoryGroupsController controller = new CategoryGroupsController(unitOfWorkMock.Object);

            var result = controller.Index("") as ViewResult;

            Assert.NotNull(result);
        }

        [Fact]
        public async void EditGetTest()
        {
            CategoryGroups CategoryGroups = new CategoryGroups { CateGroupCode = "o1", CateGroupName = "Ốc vít" };
            var mockres = new Mock<IGenericRepository<CategoryGroups>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(CategoryGroups);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.CategoryGroupsRepository).Returns(mockres.Object);

            CategoryGroupsController controller = new CategoryGroupsController(unitOfWorkMock.Object);

            var result = controller.Edit(1) as ViewResult;
            var rsmodl = result.Model as CategoryGroups;

            Assert.Equal(rsmodl.CateGroupName, CategoryGroups.CateGroupName);
            Assert.Equal(rsmodl.CateGroupCode, CategoryGroups.CateGroupCode);
        }

        [Fact]
        public async void CreateGetTest()
        {
            CategoryGroups CategoryGroups = new CategoryGroups { CateGroupCode = "o1", CateGroupName = "Ốc vít" };
            var mockres = new Mock<IGenericRepository<CategoryGroups>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(CategoryGroups);
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.CategoryGroupsRepository).Returns(mockres.Object);

            CategoryGroupsController controller = new CategoryGroupsController(unitOfWorkMock.Object);

            var result = controller.Create() as ViewResult;
            Assert.NotNull(result);

        }
    }
}
