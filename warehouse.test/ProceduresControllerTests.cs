﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using netCoreTutorial.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using WareHouse.Controllers;
using WareHouse.Models;
using Xunit;

namespace warehouse.test
{
    public class ProceduresControllerTests
    {



        [Fact]
        public async void IndexTest()
        {
            List<Procedures> getList = new List<Procedures>();
            IEnumerable<Procedures> lists = getList;
            var mockres = new Mock<IGenericRepository<Procedures>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.ProceduresRepository).Returns(mockres.Object);

            ProceduresController controller = new ProceduresController(unitOfWorkMock.Object);

            var result = controller.Index("") as ViewResult;

            Assert.NotNull(result);
        }

        [Fact]
        public async void EditGetTest()
        {
            Procedures Procedures = new Procedures { ProcedureCode = "L02", ProcedureName = "Lô" };
            var mockres = new Mock<IGenericRepository<Procedures>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(Procedures);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.ProceduresRepository).Returns(mockres.Object);

            ProceduresController controller = new ProceduresController(unitOfWorkMock.Object);

            var result = controller.Edit(1) as ViewResult;
            var rsmodl = result.Model as Procedures;

            Assert.Equal(rsmodl.ProcedureName, Procedures.ProcedureName);
            Assert.Equal(rsmodl.ProcedureCode, Procedures.ProcedureCode);
        }

        [Fact]
        public async void CreateGetTest()
        {
            Procedures Procedures = new Procedures { ProcedureCode = "SM", ProcedureName = "Nhỏ" };
            var mockres = new Mock<IGenericRepository<Procedures>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(Procedures);
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.ProceduresRepository).Returns(mockres.Object);

            ProceduresController controller = new ProceduresController(unitOfWorkMock.Object);

            var result = controller.Create() as ViewResult;
            Assert.NotNull(result);

        }
    }
}
