﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using netCoreTutorial.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using WareHouse.Controllers;
using WareHouse.Models;
using Xunit;

namespace warehouse.test
{
    public class UnitsMetricsControllerTests
    {



        [Fact]
        public async void IndexTest()
        {
            List<UnitsMetrics> getList = new List<UnitsMetrics>();
            IEnumerable<UnitsMetrics> lists = getList;
            var mockres = new Mock<IGenericRepository<UnitsMetrics>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.UnitMetricsRepository).Returns(mockres.Object);

            UnitsMetricsController controller = new UnitsMetricsController(unitOfWorkMock.Object);

            var result = controller.Index("") as ViewResult;

            Assert.NotNull(result);
        }

        [Fact]
        public async void EditGetTest()
        {
            UnitsMetrics UnitsMetrics = new UnitsMetrics { UnitsCode = "G0", UnitsName = "Cái" };
            var mockres = new Mock<IGenericRepository<UnitsMetrics>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(UnitsMetrics);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.UnitMetricsRepository).Returns(mockres.Object);

            UnitsMetricsController controller = new UnitsMetricsController(unitOfWorkMock.Object);

            var result = controller.Edit(1) as ViewResult;
            var rsmodl = result.Model as UnitsMetrics;

            Assert.Equal(rsmodl.UnitsName, UnitsMetrics.UnitsName);
            Assert.Equal(rsmodl.UnitsCode, UnitsMetrics.UnitsCode);
        }

        [Fact]
        public async void CreateGetTest()
        {
            UnitsMetrics UnitsMetrics = new UnitsMetrics { UnitsCode = "SM", UnitsName = "Nhỏ" };
            var mockres = new Mock<IGenericRepository<UnitsMetrics>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(UnitsMetrics);
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.UnitMetricsRepository).Returns(mockres.Object);

            UnitsMetricsController controller = new UnitsMetricsController(unitOfWorkMock.Object);

            var result = controller.Create() as ViewResult;
            Assert.NotNull(result);

        }
    }
}
