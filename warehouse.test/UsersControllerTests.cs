﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Moq;
using netCoreTutorial.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using WareHouse.Controllers;
using WareHouse.Models;
using Xunit;

namespace warehouse.test
{
    public class UsersControllerTests
    {

       

        [Fact]
        public async void IndexTest()
        {
            List<Users> getList = new List<Users>();
            IEnumerable<Users> lists = getList;
            var hostEnvironment = new Mock<IWebHostEnvironment>();
            var mockres = new Mock<IGenericRepository<Users>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.UsersRepository).Returns(mockres.Object);

            UsersController controller = new UsersController(unitOfWorkMock.Object,hostEnvironment.Object);

            var result = controller.Index() as ViewResult;

            Assert.Null(result);
        }

        [Fact]
        public async void EditGetTest()
        {
            Users Users = new Users { Usersname = "lekhachuy555", Email = "lightof99@gmail.com" };
            string id = "3edeb976-55be-43b3-b910-cb36a508be37";
            var mockres = new Mock<IGenericRepository<Users>>();
            var hostEnvironment = new Mock<IWebHostEnvironment>();
            mockres.Setup(x => x.GetOne(x=> x.Id.Equals(id))).Returns(Users);
            hostEnvironment.Setup(m => m.ApplicationName)
                    .Returns("WareHouse");
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.UsersRepository).Returns(mockres.Object);

            UsersController controller = new UsersController(unitOfWorkMock.Object,hostEnvironment.Object);
           
            var result = controller.Details(id) as ViewResult;
           

            Assert.NotNull(result);
            var mod = result.Model as Users;
            Assert.Equal(Users.Email, mod.Email);
            Assert.Equal(Users.Usersname, mod.Usersname);
        }

        [Fact]
        public async void CreateGetTest()
        {
            Users Users = new Users { Usersname = "lekhachuy555", Email = "lightof99@gmail.com" };
            var mockres = new Mock<IGenericRepository<Users>>();
            var hostEnvironment = new Mock<IWebHostEnvironment>();
            mockres.Setup(x => x.GetOne(x => x.Id.Equals("3edeb976-55be-43b3-b910-cb36a508be37"))).Returns(Users);
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.UsersRepository).Returns(mockres.Object);

            UsersController controller = new UsersController(unitOfWorkMock.Object, hostEnvironment.Object);

            var result = controller.Details("3edeb976-55be-43b3-b910-cb36a508be37") as ViewResult;
            Assert.Null(result);

        }
    }
}
