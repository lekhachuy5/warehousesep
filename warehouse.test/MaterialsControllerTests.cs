﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Moq;
using netCoreTutorial.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using WareHouse.Controllers;
using WareHouse.Models;
using Xunit;

namespace warehouse.test
{
    public class MaterialsControllerTests
    {



        [Fact]
        public async void IndexTest()
        {
            List<Materials> getList = new List<Materials>();
            IEnumerable<Materials> lists = getList;
            var mockres = new Mock<IGenericRepository<Materials>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);
            var mockress = new Mock<IWebHostEnvironment>();
            mockress
    .               Setup(m => m.ApplicationName)
                    .Returns("WareHouse");
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.MaterialsRepository).Returns(mockres.Object);

            MaterialsController controller = new MaterialsController(unitOfWorkMock.Object, mockress.Object);

            var result = controller.Index("") as ViewResult;

            Assert.NotNull(result);
        }


    }
}
