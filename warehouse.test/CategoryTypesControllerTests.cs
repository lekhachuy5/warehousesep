﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using netCoreTutorial.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using WareHouse.Controllers;
using WareHouse.Models;
using Xunit;

namespace warehouse.test
{
    public class CategoryTypesControllerTests
    {



        [Fact]
        public async void IndexTest()
        {
            List<CategoryTypes> getList = new List<CategoryTypes>();
            IEnumerable<CategoryTypes> lists = getList;
            var mockres = new Mock<IGenericRepository<CategoryTypes>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.CategoryTypesRepository).Returns(mockres.Object);

            CategoryTypesController controller = new CategoryTypesController(unitOfWorkMock.Object);

            var result = controller.Index("") as ViewResult;

            Assert.NotNull(result);
        }

        [Fact]
        public async void EditGetTest()
        {
            CategoryTypes CategoryTypes = new CategoryTypes { CateTypeCode = "TC", CateTypeName = "Ốc vít 3mm" , Group_Id = 1 , CateLocation =1};
            CategoryGroups CategoryGroups = new CategoryGroups { CateGroupCode = "o1", CateGroupName = "Ốc vít" };
            var mockress = new Mock<IGenericRepository<CategoryGroups>>();
            mockress.Setup(x => x.GetByIdAsync(1)).Returns(CategoryGroups);

            var mockres = new Mock<IGenericRepository<CategoryTypes>>();
            mockres.Setup(x => x.GetByIdAsync(7)).Returns(CategoryTypes);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.CategoryTypesRepository).Returns(mockres.Object);
            unitOfWorkMock.Setup(m => m.CategoryGroupsRepository).Returns(mockress.Object);
            CategoryTypesController controller = new CategoryTypesController(unitOfWorkMock.Object);

            var result = controller.Edit(7) as ViewResult;
            var rsmodl = result.Model as CategoryTypes;

            Assert.Equal(rsmodl.CateTypeName, CategoryTypes.CateTypeName);
            Assert.Equal(rsmodl.CateTypeCode, CategoryTypes.CateTypeCode);
        }

        [Fact]
        public async void CreateGetTest()
        {
            CategoryTypes CategoryTypes = new CategoryTypes { CateTypeCode = "TC", CateTypeName = "Ốc vít 3mm" };
            CategoryGroups CategoryGroups = new CategoryGroups { CateGroupCode = "o1", CateGroupName = "Ốc vít" };
            var mockress = new Mock<IGenericRepository<CategoryGroups>>();
            mockress.Setup(x => x.GetByIdAsync(1)).Returns(CategoryGroups);
            var mockres = new Mock<IGenericRepository<CategoryTypes>>();
            mockres.Setup(x => x.GetByIdAsync(7)).Returns(CategoryTypes);
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.CategoryTypesRepository).Returns(mockres.Object);
            unitOfWorkMock.Setup(m => m.CategoryGroupsRepository).Returns(mockress.Object);
            CategoryTypesController controller = new CategoryTypesController(unitOfWorkMock.Object);

            var result = controller.Create() as ViewResult;
            Assert.NotNull(result);

        }
    }
}
