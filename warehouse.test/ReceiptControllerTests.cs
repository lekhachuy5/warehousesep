﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using netCoreTutorial.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using WareHouse.Controllers;
using WareHouse.Models;
using Xunit;

namespace warehouse.test
{
    public class ReceiptControllerTests
    {



        [Fact]
        public async void IndexTest1()
        {
            List<Receipts> getList = new List<Receipts>();
            IEnumerable<Receipts> lists = getList;
            var mockres = new Mock<IGenericRepository<Receipts>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.ReceiptRepository).Returns(mockres.Object);

            ReceiptController controller = new ReceiptController(unitOfWorkMock.Object);

            var result = controller.Imports("",null) as ViewResult;

            Assert.NotNull(result);
        }
        [Fact]
        public async void IndexTest2()
        {
            List<Receipts> getList = new List<Receipts>();
            IEnumerable<Receipts> lists = getList;
            var mockres = new Mock<IGenericRepository<Receipts>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.ReceiptRepository).Returns(mockres.Object);

            ReceiptController controller = new ReceiptController(unitOfWorkMock.Object);

            var result = controller.ReImports("", null) as ViewResult;

            Assert.NotNull(result);
        }

        [Fact]
        public async void IndexTest()
        {
            List<Receipts> getList = new List<Receipts>();
            IEnumerable<Receipts> lists = getList;
            var mockres = new Mock<IGenericRepository<Receipts>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.ReceiptRepository).Returns(mockres.Object);

            ReceiptController controller = new ReceiptController(unitOfWorkMock.Object);

            var result = controller.Exports("", null) as ViewResult;

            Assert.NotNull(result);
        }


    }
}
