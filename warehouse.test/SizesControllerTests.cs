﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using netCoreTutorial.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using WareHouse.Controllers;
using WareHouse.Models;
using Xunit;

namespace warehouse.test
{
    public class SizesControllerTests
    {



        [Fact]
        public async void IndexTest()
        {
            List<Sizes> getList = new List<Sizes>();
            IEnumerable<Sizes> lists = getList;
            var mockres = new Mock<IGenericRepository<Sizes>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.SizesRepository).Returns(mockres.Object);

            SizesController controller = new SizesController(unitOfWorkMock.Object);

            var result = controller.Index("") as ViewResult;

            Assert.NotNull(result);
        }

        [Fact]
        public async void EditGetTest()
        {
            Sizes sizes = new Sizes { SizeCode = "SM", SizeName = "Nhỏ" };
            var mockres = new Mock<IGenericRepository<Sizes>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(sizes);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.SizesRepository).Returns(mockres.Object);

            SizesController controller = new SizesController(unitOfWorkMock.Object);

            var result = controller.Edit(1) as ViewResult;
            var rsmodl = result.Model as Sizes;

            Assert.Equal(rsmodl.SizeName, sizes.SizeName);
            Assert.Equal(rsmodl.SizeCode, sizes.SizeCode);
        }

        [Fact]
        public async void CreateGetTest()
        {
            Sizes sizes = new Sizes { SizeCode = "SM", SizeName = "Nhỏ" };
            var mockres = new Mock<IGenericRepository<Sizes>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(sizes);
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.SizesRepository).Returns(mockres.Object);

            SizesController controller = new SizesController(unitOfWorkMock.Object);

            var result = controller.Create() as ViewResult;
            Assert.NotNull(result);

        }
    }
}
