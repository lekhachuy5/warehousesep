﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using netCoreTutorial.DAL;
using System;
using WareHouse.Controllers;
using WareHouse.Migrations;
using WareHouse.Models;
using Xunit;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace warehouse.test
{
    public class BrandControllerTest
    {
       
     

        [Fact]
        public async void IndexTest()
        {
            List<Brands> brands = new List<Brands>();
            IEnumerable<Brands> brand = brands;
            var mockres = new Mock<IGenericRepository<Brands>>();
            mockres.Setup(x => x.Get(null,null,"")).Returns(brand);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.BrandsRepository).Returns(mockres.Object);

            BrandsController controller = new BrandsController(unitOfWorkMock.Object);

            var result = controller.Index("") as ViewResult;
          
            Assert.NotNull(result);
        }

        [Fact]
        public async void EditGetTest()
        {
            Brands Brands = new Brands { BrandCode = "S0", BrandName = "Sunco" };
            var mockres = new Mock<IGenericRepository<Brands>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(Brands);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.BrandsRepository).Returns(mockres.Object);

            BrandsController controller = new BrandsController(unitOfWorkMock.Object);

            var result = controller.Edit(1) as ViewResult;
            var rsmodl = result.Model as Brands;

            Assert.Equal(rsmodl.BrandName, Brands.BrandName);
            Assert.Equal(rsmodl.BrandCode, Brands.BrandCode);
        }

        [Fact]
        public async void CreateGetTest()
        {
            Brands Brands = new Brands { BrandCode = "S0", BrandName = "Sunco" };
            var mockres = new Mock<IGenericRepository<Brands>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(Brands);
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.BrandsRepository).Returns(mockres.Object);

            BrandsController controller = new BrandsController(unitOfWorkMock.Object);

            var result = controller.Create() as ViewResult;
            Assert.NotNull(result);

        }
    }
}
