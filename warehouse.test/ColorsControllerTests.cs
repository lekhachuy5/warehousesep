﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using netCoreTutorial.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using WareHouse.Controllers;
using WareHouse.Models;
using Xunit;

namespace warehouse.test
{
    public class ColorsControllerTests
    {



        [Fact]
        public async void IndexTest()
        {
            List<Colors> getList = new List<Colors>();
            IEnumerable<Colors> lists = getList;
            var mockres = new Mock<IGenericRepository<Colors>>();
            mockres.Setup(x => x.Get(null, null, "")).Returns(lists);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.ColorsRepository).Returns(mockres.Object);

            ColorsController controller = new ColorsController(unitOfWorkMock.Object);

            var result = controller.Index("") as ViewResult;

            Assert.NotNull(result);
        }

        [Fact]
        public async void EditGetTest()
        {
            Colors Colors = new Colors { ColorCode = "Nâu", ColorName = "VR" };
            var mockres = new Mock<IGenericRepository<Colors>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(Colors);

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.ColorsRepository).Returns(mockres.Object);

            ColorsController controller = new ColorsController(unitOfWorkMock.Object);

            var result = controller.Edit(1) as ViewResult;
            var rsmodl = result.Model as Colors;

            Assert.Equal(rsmodl.ColorName, Colors.ColorName);
            Assert.Equal(rsmodl.ColorCode, Colors.ColorCode);
        }

        [Fact]
        public async void CreateGetTest()
        {
            Colors Colors = new Colors { ColorCode = "Nâu", ColorName = "VR" };
            var mockres = new Mock<IGenericRepository<Colors>>();
            mockres.Setup(x => x.GetByIdAsync(1)).Returns(Colors);
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(m => m.ColorsRepository).Returns(mockres.Object);

            ColorsController controller = new ColorsController(unitOfWorkMock.Object);

            var result = controller.Create() as ViewResult;
            Assert.NotNull(result);

        }
    }
}
